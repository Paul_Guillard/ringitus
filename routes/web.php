<?php

use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SoundsController;
use App\Livewire\Auth\Login;
use App\Livewire\Auth\Passwords\Confirm;
use App\Livewire\Auth\Passwords\Email;
use App\Livewire\Auth\Passwords\Reset;
use App\Livewire\Auth\Register;
use App\Livewire\Auth\Verify;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'showRingitus'])->name('home');
Route::get('reveil-matin-acouphenes', [HomeController::class, 'showRingitus'])->name('ringitus');
Route::get('reveil-matin-acouphenes/echantillons', [SoundsController::class, 'index'])->name('sounds.index');
Route::get('nous-contacter', [GeneralController::class, 'showContact'])->name('contact');
Route::get('conditions-generales-d-utilisation', [GeneralController::class, 'showConditions'])->name('general-conditions');
Route::get('mentions-legales', [GeneralController::class, 'showLegal'])->name('legal-mentions');
Route::get('politique-de-confidentialite', [GeneralController::class, 'showDataPolicy'])->name('data-policy');

Route::middleware('guest')->group(function () {
    Route::get('connexion', Login::class)
        ->name('login');

    Route::get('inscription', Register::class)
        ->name('register');
});

Route::get('mot-de-passe/reset', Email::class)
    ->name('password.request');

Route::get('mot-de-passe/reset/{token}', Reset::class)
    ->name('password.reset');

Route::middleware('auth')->group(function () {
    Route::get('email/verify/{id}/{hash}', EmailVerificationController::class)
        ->middleware('signed')
        ->name('verification.verify');

    Route::post('logout', LogoutController::class)
        ->name('logout');

    Route::get('email/verify', Verify::class)
        ->middleware('throttle:6,1')
        ->name('verification.notice');

    Route::get('mot-de-passe/confirmation', Confirm::class)
        ->name('password.confirm');

    Route::get('tableau-de-bord', [DashboardController::class, 'show'])->name('dashboard');
});
