@extends('layouts.app')

@section('title')
RG EIRL - Outils diagnostics pour les acouphènes
@endsection

@section('description')
RG EIRL propose des solutions pour soulager les personnes souffrant d'acouphènes, notamment des outils diagnostics et thérapeutiques.
@endsection

@section('content')
    <section class="relative w-full pt-20" id="hero">
        <div class="relative min-h-[100svh] lg:min-h-[100vh]">
            <img src="{{ asset('assets/pictures/home-banner.jpeg') }}" alt="Photo bannière acouphènes et sommeil" class="absolute top-0 left-0 z-0 object-cover w-full h-full">
            <div class="absolute top-0 left-0 z-10 w-full h-full bg-slate-800 opacity-40"></div>
            <div class="container relative z-10 pt-28">
                <h1 class="mb-12 text-3xl font-bold text-center text-white lg:text-left lg:text-6xl">
                    Des outils diagnostics et thérapeutiques <br/>pour aider les personnes souffrant d'acouphènes
                </h1>
                <h2 class="w-full mb-12 text-base italic font-light text-center text-white lg:text-left lg:w-11/12 lg:text-lg">
                    Nous travaillons à développer les outils de demain pour mieux prendre en charge
                    les personnes souffrant d'acouphènes et d'hyperacousie.
                    Nous explorons plus spécifiquement les interactions entre acouphènes et sommeil.<br/>
                    Vous êtes concerné.e si vous avez des variations importantes de l'intensité
                    de vos acouphènes suite à des siestes ou à vos nuits de sommeil.
                </h2>
                <div class="flex flex-wrap justify-center pb-12">
                    <a href="{{ route('ringitus') }}" class="inline-block w-full px-5 py-3 mx-5 mb-5 text-center text-white transition duration-300 rounded lg:mb-0 lg:w-fit bg-sky-500 hover:bg-white hover:text-sky-500">Créer un réveil anti-acouphènes</a>
                    <a href="{{ route('contact') }}" class="inline-block w-full px-5 py-3 mx-5 text-center transition duration-300 bg-white rounded lg:w-fit text-sky-500 hover:text-white hover:bg-sky-500">Nous contacter</a>
                </div>
            </div>
        </div>
    </section>

    <section class="py-16">
        <h3 class="mb-3 text-lg font-medium tracking-wider text-center uppercase text-slate-500">
            Ce que nous faisons
        </h3>
        <h2 class="mb-10 text-4xl font-extrabold tracking-wide text-center uppercase text-slate-800">
            Notre technologie
        </h2>
        <p class="w-3/4 mx-auto text-lg leading-7 text-center lg:leading-9 lg:text-xl mb-36 text-slate-500">
            Nous travaillons avec les patients pour déterminer les causes physiologiques de leur acouphène. Pour cela, nous développons des méthodes de traitement du signal et d’apprentissage automatique aux service des praticiens afin de faciliter leur diagnostic.
        </p>

        <h3 class="mb-3 text-lg font-medium tracking-wider text-center uppercase text-slate-500">
            Ce que nous sommes
        </h3>
        <h2 class="mb-10 text-4xl font-extrabold tracking-wide text-center uppercase text-slate-800">
            Notre équipe
        </h2>
        <p class="w-3/4 mx-auto mb-12 text-lg leading-7 text-center lg:text-xl lg:leading-9 text-slate-500">
            Notre équipe dédiée à la recherche sur l’acouphène travaille en collaboration
            avec des chercheurs et des praticiens. Notre approche est pluridisciplinaire&nbsp;:
            nous travaillons directement avec les personnes touchées par l’acouphène afin de comprendre
            et interpréter les symptômes et chercher des solutions les plus adaptées.
            Notre technologie repose sur l’analyse des données physiologiques afin de diagnostiquer
            plus précisément la cause de l’acouphène.
        </p>
        {{-- <div class="text-center">
            <figure>
                <img src="{{ asset('assets/pictures/portrait_RG.jpg') }}" alt="Portrait Robin Guillard" class="mx-auto">
                <figcaption class="block pt-5">
                    <p class="text-lg font-bold tracking-wider text-center uppercase text-slate-800">Robin Guillard</p>
                    <div class="w-24 mx-auto my-2 border-b border-slate-300"></div>
                    <p class="text-sm tracking-wide text-slate-500">CEO</p>
                </figcaption>
            </figure>
        </div> --}}
    </section>
@endsection
