@extends('layouts.base')

@section('body')
    <div class="relative" x-data="{ mobileMenu: false }">
        <div class="block lg:hidden">
            @include('includes.layout.mobile-menu')
            @include('includes.layout.mobile-nav')
        </div>
        @include('includes.layout.desktop-nav')

        @yield('content')

        @isset($slot)
            {{ $slot }}
        @endisset

        @include('includes.layout.footer')
    </div>
@endsection
