<div>
    @forelse ($rings as $ring)
        <div wire:key='ring-{{ $ring->id }}' class="flex flex-wrap w-full px-1 py-2 border-b border-slate-200 lg:justify-between">
            <div class="w-full lg:w-1/4">
                <input type="text" class="w-full text-center border-none shadow-none outline-none lg:text-left focus:outline-none focus:border-none focus:ring-transparent" wire:model.lazy='names.{{ $ring->id }}' maxlength="150">
            </div>
            <div class="w-full pt-2 mb-4 text-center lg:text-left lg:w-1/4 lg:mb-0">
                Créée le {{ Carbon\Carbon::parse($ring->created_at)->format('d/m/Y') }}
            </div>
            <div class="w-1/2 mx-auto lg:w-1/4">
                @include('includes.components.audio-player', [
                            'soundId' => "user-ring-" . $ring->id,
                            'src' => asset('storage/' . $ring->file),
                            'withTime' => false,
                            'duration' => 180,
                        ])
            </div>
            <div class="w-full text-center lg:text-left lg:w-1/4" x-data="{ confirmation: false }">
                <div x-show="confirmation">
                    Supprimer ?
                    <button class="px-4 py-2 mx-1 my-1 text-white transition rounded bg-sky-500 hover:bg-green-400" @click="confirmation = false">Annuler</button>
                    <button class="px-4 py-2 mx-1 my-1 text-white transition bg-red-500 rounded hover:bg-red-600" wire:click='deleteRing({{ $ring->id }})'>Confirmer</button>
                </div>
                <div x-show="!confirmation">
                    <button class="px-4 py-2 mx-1 my-1 text-white transition rounded bg-sky-500 hover:bg-green-400" wire:click='downloadRing({{ $ring->id }})'>Télécharger</button>
                    <button class="px-4 py-2 mx-1 my-1 text-white transition bg-red-500 rounded hover:bg-red-600" @click="confirmation = true">Supprimer</button>
                </div>
            </div>
        </div>
    @empty
        <div>
            <p class="mb-5 text-center lg:text-left"><em>Aucune sonnerie créée pour le moment...</em></p>
            <a href="{{ route('ringitus') }}" class="inline-block px-5 py-3 text-center text-white transition duration-300 rounded bg-sky-500 hover:bg-green-400">Créer une sonnerie</a>
        </div>
    @endforelse
</div>
