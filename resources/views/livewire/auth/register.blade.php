@section('title', 'Create a new account')

<div class="pt-28">
    <div class="sm:mx-auto sm:w-full sm:max-w-md">
        <a href="{{ route('home') }}" class="inline-block w-full text-2xl text-center">
            <img src="{{ asset('assets/pictures/logo_ringitus.png') }}" alt="Logo Ringitus" class="h-16 mx-auto">
        </a>

        <h2 class="mt-6 text-3xl font-extrabold leading-9 text-center text-gray-900">
            Créer un compte
        </h2>

        <p class="mt-2 text-sm leading-5 text-center text-gray-600 max-w">
            Ou
            <a href="{{ route('login') }}" class="font-medium transition duration-150 ease-in-out text-sky-500 hover:text-green-400 focus:outline-none focus:underline">
                connectez-vous à votre compte
            </a>
        </p>
    </div>

    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div class="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">
            <form wire:submit.prevent="register" x-data="{ step: @entangle('step') }">
                @csrf
                @error('answers')
                {{ $message }}
                @enderror
                <div x-show="step === 0">
                    <div>
                        <label for="firstName" class="block text-sm font-medium leading-5 text-gray-700">
                            Prénom
                        </label>

                        <div class="mt-1 rounded-md shadow-sm">
                            <input wire:model="firstName" id="firstName" type="text" required autofocus class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:ring-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('name') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:ring-red @enderror" />
                        </div>

                        @error('firstName')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mt-6">
                        <label for="lastName" class="block text-sm font-medium leading-5 text-gray-700">
                            Nom
                        </label>

                        <div class="mt-1 rounded-md shadow-sm">
                            <input wire:model="lastName" id="lastName" type="text" required autofocus class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:ring-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('name') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:ring-red @enderror" />
                        </div>

                        @error('lastName')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mt-6">
                        <label for="birth" class="block text-sm font-medium leading-5 text-gray-700">
                            Date de naissance
                        </label>

                        <div class="mt-1 rounded-md shadow-sm">
                            <input wire:model="dateOfBirth" id="birth" type="date" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:ring-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('dateOfBirth') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:ring-red @enderror" />
                        </div>

                        @error('dateOfBirth')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mt-6">
                        <label for="email" class="block text-sm font-medium leading-5 text-gray-700">
                            Adresse e-mail
                        </label>

                        <div class="mt-1 rounded-md shadow-sm">
                            <input wire:model="email" id="email" type="email" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:ring-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('email') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:ring-red @enderror" />
                        </div>

                        @error('email')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mt-6">
                        <label for="phone" class="block text-sm font-medium leading-5 text-gray-700">
                            Numéro de téléphone
                        </label>

                        <div class="mt-1 rounded-md shadow-sm">
                            <input wire:model="phone" id="phone" type="text" minlength="8" maxlength="20" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:ring-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('phone') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:ring-red @enderror" />
                        </div>

                        @error('phone')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mt-6">
                        <label for="password" class="block text-sm font-medium leading-5 text-gray-700">
                            Mot de passe
                        </label>

                        <div class="relative mt-1 rounded-md shadow-sm" x-data="{ visible: false }">
                            <input wire:model="password" id="password" :type="visible ? 'text' : 'password'" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:ring-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('password') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:ring-red @enderror" />
                            <div class="absolute top-2 right-2">
                                <i class="transition cursor-pointer fa-regular text-slate-700 hover:text-sky-500" :class="visible ? 'fa-eye-slash' : 'fa-eye'" @click="visible = !visible"></i>
                            </div>
                        </div>

                        @error('password')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mt-6">
                        <label for="password_confirmation" class="block text-sm font-medium leading-5 text-gray-700">
                            Confirmation du mot de passe
                        </label>

                        <div class="relative mt-1 rounded-md shadow-sm" x-data="{ visible: false }">
                            <input wire:model="passwordConfirmation" id="password_confirmation" :type="visible ? 'text' : 'password'" required class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none focus:outline-none focus:ring-blue focus:border-blue-300 sm:text-sm sm:leading-5" />
                            <div class="absolute top-2 right-2">
                                <i class="transition cursor-pointer fa-regular text-slate-700 hover:text-sky-500" :class="visible ? 'fa-eye-slash' : 'fa-eye'" @click="visible = !visible"></i>
                            </div>
                        </div>
                    </div>

                    <div class="mt-6">
                        <span class="block w-full rounded-md shadow-sm">
                            <button wire:click.prevent.stop="checkPersonalInfos" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white transition duration-300 border border-transparent rounded-md bg-sky-500 hover:bg-green-400 focus:outline-none focus:border-sky-500 focus:ring-indigo active:bg-sky-500">
                                Continuer l'inscription
                            </button>
                        </span>
                    </div>
                </div>

                <div x-show="step === 1">
                    <div class="mt-6">
                        <p class="mb-4 text-lg text-center">
                            <em>
                                Encore une petite étape...
                            </em>
                        </p>
                        <p class="mb-4 text-sm text-center">
                                Notre outil est encore en phase de test, aussi nous avons besoin de vos retours pour savoir s'il fonctionne bien pour aider les personnes souffrant d'acouphènes. Pour y parvenir, nous aurions besoin d'évaluer comment évoluent vos acouphènes à partir du moment où vous avez commencé à utiliser les pistes sonores de votre réveil anti-acouphène.
                        </p>
                        <p class="text-lg text-center">
                            <em>
                                Pour nous y aider et pour finaliser la création de votre compte, merci de répondre aux questions qui suivent.
                            </em>
                        </p>
                    </div>
                    <div class="my-6">
                        <span class="block w-full rounded-md shadow-sm">
                            <button @click.prevent.stop="step = 2" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white transition duration-300 border border-transparent rounded-md bg-sky-500 hover:bg-green-400 focus:outline-none focus:border-sky-500 focus:ring-indigo active:bg-sky-500">
                                Répondre aux questions
                            </button>
                        </span>
                    </div>
                    <div class="text-sm">
                        <button @click.prevent.stop="step--" class="transition hover:text-sky-500">
                            <i class="mr-2 fa-solid fa-arrow-left"></i> Précédent
                        </button>
                    </div>
                </div>

                <div x-show="step >= 2 && step < 2 + @js($questions->count())">
                    @foreach ($questions as $question)
                        <div wire:key='question-{{ $question->id }}' x-show="step === (2 + {{ $loop->index }})">
                            <p class="mb-4 text-lg">
                                {{ ucfirst($question->question) }}
                            </p>
                            @if ($question->type === App\Enums\QuestionTypes::MultipleChoice)
                                @foreach ($question->options as $option)
                                    <div wire:key='answer-{{ $question->id }}-{{ $option['key'] }}' class="mb-2">
                                        <label>
                                            <input type="checkbox" wire:model.live="answers.{{ $question->id }}.{{ $option['key'] }}" name="answers[{{ $question->id }}]" value="{{ $option['key'] }}" class="mr-2 rounded text-sky-500 focus:outline-none focus:shadow-none" required>
                                            {{ $option['answer'] }}
                                        </label>
                                    </div>
                                @endforeach
                            @elseif ($question->type === App\Enums\QuestionTypes::Radio)
                                @foreach ($question->options as $option)
                                    <div wire:key='answer-{{ $question->id }}-{{ $option['key'] }}' class="mb-2">
                                        <label>
                                            <input type="radio" wire:model.live="answers.{{ $question->id }}" name="answers[{{ $question->id }}]" value="{{ $option['answer'] }}" class="mr-2 rounded text-sky-500 focus:outline-none focus:shadow-none" required>
                                            {{ $option['answer'] }}
                                        </label>
                                    </div>
                                @endforeach
                            @else
                                <div class="flex justify-between" x-data="{ squareVisible: false }">
                                    <input type="range" min="0" max="10" value="5" step="1" wire:model.lazy='answers.{{ $question->id }}' class="w-4/5 neutral-range" @change="squareVisible = true">
                                    <div x-show="squareVisible" class="w-8 h-8 py-1 font-medium text-center text-white border rounded aspect-square border-sky-500 bg-sky-500">
                                        {{ $answers[$question->id] ?? '5' }}
                                    </div>
                                </div>
                            @endif

                            <div class="my-6">
                                <span class="block w-full rounded-md shadow-sm">
                                    @if ($loop->last)
                                        <button @click.prevent.stop="step++" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white transition duration-300 border border-transparent rounded-md bg-sky-500 hover:bg-green-400 focus:outline-none focus:border-sky-500 focus:ring-indigo active:bg-sky-500">
                                            Finaliser la création du compte
                                        </button>
                                    @else
                                    <button @click.prevent.stop="step++" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white transition duration-300 border border-transparent rounded-md bg-sky-500 hover:bg-green-400 focus:outline-none focus:border-sky-500 focus:ring-indigo active:bg-sky-500">
                                        Question suivante
                                    </button>
                                    @endif
                                    <div class="my-6 text-sm">
                                        <button @click.prevent.stop="step--" class="transition hover:text-sky-500">
                                            <i class="mr-2 fa-solid fa-arrow-left"></i> Précédent
                                        </button>
                                    </div>
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div x-show="step >= 2 + @js($questions->count())">
                    <div class="mt-6">
                        <p class="mb-4 text-center">
                            Vos réponses nous permettent d'évaluer scientifiquement et cliniquement l'efficacité de notre outil pour aider les personnes souffrant d'acouphènes&nbsp;!
                        </p>
                        <div class="my-5 text-orange-700 bg-orange-100 border border-orange-500 rounded-xl p-7">
                            L'outil Ting! est actuellement en phase de test. Afin de nous aider au mieux
                            à optimiser le fonctionnement de l'outil, nous vous encourageons à accepter toutes
                            les options proposées ci-dessous. Cela nous permettra d'obtenir des données plus pertinentes.
                            Il sera toujours possible de modifier ces choix par la suite depuis votre espace utilisateur.
                        </div>
                        <div>
                            <label class="inline-block my-2">
                                <input type="checkbox" wire:model.lazy="conditionsAccepted" value='true' class="mr-2 rounded text-sky-500 focus:outline-none focus:shadow-none" required>
                                * J'accepte les <a href="{{ route('general-conditions') }}" target="_blank" class="text-sky-500 hover:underline">conditions générales d'utilisation</a>
                            </label>
                            <label class="inline-block my-2">
                                <input type="checkbox" wire:model.lazy="followupAccepted" value='true' class="mr-2 rounded text-sky-500 focus:outline-none focus:shadow-none">
                                J'accepte de recevoir des emails de RG EIRL (suivi des acouphènes, newsletter)
                            </label>
                            <label class="inline-block my-2">
                                <input type="checkbox" wire:model.lazy="publishedDataAccepted" value='true' class="mr-2 rounded text-sky-500 focus:outline-none focus:shadow-none">
                                J'accepte l'utilisation anonyme de mes réponses pour contribuer aux études scientifiques sur les acouphènes
                            </label>
                        </div>
                    </div>
                    <div class="my-6">
                        <div class='w-full mx-auto pt-7' wire:loading wire:target='register'>
                            <div class="mx-auto pulse my-7"></div>
                        </div>
                        <span class="block w-full rounded-md shadow-sm" wire:loading.remove>
                            <button type="submit" wire:click="register" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white transition duration-300 border border-transparent rounded-md bg-sky-500 hover:bg-green-400 focus:outline-none focus:border-sky-500 focus:ring-indigo active:bg-sky-500">
                                Créer le compte
                            </button>
                            @error('answers')
                                <p class="my-3 text-sm text-red-600">
                                    Certaines informations n'ont pas été correctement remplies. Merci de vérifier vos réponses aux questions : {{ $message }}
                                </p>
                            @enderror
                            @error('multiple')
                                <p class="my-3 text-sm text-red-600">
                                    {{ $message }}
                                </p>
                            @enderror
                        </span>
                    </div>
                    <div class="text-sm" wire:loading.remove>
                        <button @click.prevent.stop="step--" class="transition hover:text-sky-500">
                            <i class="mr-2 fa-solid fa-arrow-left"></i> Précédent
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
