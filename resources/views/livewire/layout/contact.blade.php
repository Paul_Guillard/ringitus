<div class="flex flex-wrap justify-between">
    <div class="flex flex-col justify-start w-full pt-8 mb-12 font-light lg:mb-0 lg:w-1/2 lg:pb-0 lg:px-10 text-slate-700">
        <p class="mb-6 text-5xl text-center text-sky-500">
            <i class="fa-solid fa-location-dot"></i>
        </p>
        <p class="mb-6 text-lg text-center">
            RG EIRL <br/>93 Cours Berriat<br/>38000 Grenoble
        </p>
        {{-- <p class="mb-6 text-5xl text-center text-sky-500">
            <i class="fa-regular fa-envelope"></i>
        </p> --}}
        <p class="text-center">Une question ? Une remarque ? N'hésitez pas à nous contacter via le formulaire de contact <span class="hidden lg:inline">ci-contre</span><span class="inline lg:hidden">ci-dessous</span>.</p>
    </div>
    <div class="w-full lg:w-1/2 lg:px-7">
        <form wire:submit.prevent='save' method="POST">
            @csrf
            <x-honeypot livewire-model="extraFields" />

            <div @class(['hidden' => auth()->check()])>
                <div class="flex flex-wrap justify-between mb-5">
                    <div class="w-full mb-5 lg:w-1/2 lg:pr-2 lg:mb-0">
                        <input type="text" wire:model='firstName' placeholder="Prénom" class="w-full rounded border-slate-400" required>
                        @error('firstName') <p class="text-red-600">{{ $message }}</p> @enderror
                    </div>
                    <div class="w-full lg:w-1/2 lg:pl-2">
                        <input type="text" wire:model='lastName' placeholder="Nom" class="w-full rounded border-slate-400" required>
                        @error('lastName') <p class="text-red-600">{{ $message }}</p> @enderror
                    </div>
                </div>
                <div class="mb-5">
                    <input type="email" wire:model='email' class="w-full rounded border-slate-400" placeholder="E-mail" required>
                    @error('email') <p class="text-red-600">{{ $message }}</p> @enderror
                </div>
            </div>
            <div>
                <textarea name="message" wire:model='message' id="message" rows="10" maxlength="2000" class="w-full p-5 rounded border-slate-400" placeholder="Votre message ici - Max 2000 caractères" required></textarea>
                @error('message') <p class="text-red-600">{{ $message }}</p> @enderror
            </div>
            <div @class(["my-5 text-center", 'hidden' => $sent])>
                <button type="submit" class="px-5 py-2 text-white transition rounded bg-sky-500 hover:bg-green-400">Envoyer</button>
            </div>
            <div @class(["my-5 text-center w-full bg-green-400 text-white p-5 rounded", 'hidden' => !$sent])>
                Votre message a été envoyé avec succès, nous vous en remercions. Nous vous répondrons dans les plus brefs délais.
            </div>
        </form>
    </div>
</div>
