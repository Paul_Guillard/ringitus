<div class="relative flex flex-wrap justify-between lg:flex-nowrap" x-data x-init="document.addEventListener('scroll', function() {
    if (window.scrollY < screen.height || document.getElementById('selection').getBoundingClientRect().top < 250) {
        document.getElementById('selection-btn').classList.add('hidden')
    } else {
        document.getElementById('selection-btn').classList.remove('hidden')
    }
})">
    <div id="selection-btn" @class(['block lg:hidden fixed bottom-6 border border-sky-500 right-2 bg-white text-center mx-auto p-3 text-sky-500 rounded-lg cursor-pointer opacity-0 -z-10 shadow', 'z-50 opacity-100' => count($selectedSounds) > 0])>
        <a href="#selection" class="inline-block w-full h-full">
            Voir ma sélection
        </a>
    </div>
    <div class="container w-full lg:w-3/4 lg:pr-7">
        @include('includes.components.sound-list', [
            'title' => 'Nos suggestions',
            'sounds' => $suggestedSounds,
            'theme' => 'suggested',
        ])

        @include('includes.components.sound-list', [
            'title' => 'Sons de la nature',
            'sounds' => $natureSounds,
            'credits' => 'Ces sons vous sont proposés par <a href="https://lasonotheque.org/search?q=nature" class="underline" target="_blank">La Sonothèque</a>',
            'theme' => 'nature',
        ])

        @include('includes.components.sound-list', [
            'title' => 'Bruits blancs',
            'sounds' => $tinnitusSounds,
            'theme' => 'wave',
        ])
    </div>

    <div class="w-full lg:w-1/4 lg:px-7 min-w-[270px]" id="selection">
        <div class="sticky w-full border top-20 border-slate-200 rounded-xl p-7 lg:max-h-[85vh] overflow-y-auto">
            @guest
            <div class="pt-5 text-center">
                <p>
                    <em><a href="{{ route('register') }}" class="text-sky-500 hover:underline">Créez un compte</a> ou <a href="{{ route('login') }}" class="text-sky-500 hover:underline">connectez-vous</a> pour créer votre sonnerie</em>
                </p>
            </div>
            @else
            <div @class(["mb-5", 'hidden' => $ring]) wire:loading.remove wire:target='generateRing'>
                <h3 class="pb-4 text-lg font-medium text-center text-sky-500">
                    Ma sélection
                </h3>
                @forelse ($selectedSounds as $selectedSound)
                    <div wire:key='selected-{{ $selectedSound->id }}' class="p-3 mb-2 rounded-lg bg-slate-200">
                        <div class="flex justify-between">
                            <p class="mb-5">{{ $selectedSound->name }}</p>
                            <button class="block transition hover:text-red-600 h-fit" wire:click='removeSound({{ $selectedSound->id }})'>
                                <i class="fa-regular fa-circle-xmark"></i>
                            </button>
                        </div>
                        <div class="flex">
                            <label for="volume-{{ $selectedSound->id }}" class="block mr-5 text-sm">Volume</label>
                            <div class="px-2">
                                <input type="range" class="w-full" id="volume-{{ $selectedSound->id }}" wire:model='selectedVolumes.{{ $selectedSound->id }}' min="0" max="10" />
                            </div>
                        </div>
                    </div>
                @empty
                    <p class="text-sm text-center"><em>Sélectionnez un son dans la liste</em></p>
                @endforelse
            </div>

            <div @class(["mb-5 pt-4", 'hidden' => $ring]) wire:loading.remove wire:target='generateRing' x-data="{ withWakeUp: false }">
                <h3 class="pb-4 text-lg font-medium text-center cursor-pointer text-sky-500" @click="withWakeUp = !withWakeUp">
                    <i class="mr-1 transition fa-solid fa-chevron-down" :class="withWakeUp ? 'rotate-180' : 'rotate-0'"></i> Ajouter un réveil ?
                </h3>
                <div class="w-full min-h-[80px]" x-show="withWakeUp" x-transition>
                    @forelse ($wakeupSounds as $wakeupSound)
                        <div wire:key='wakeup-{{ $wakeupSound->id }}' @class(['border-b border-white flex justify-between p-2 pr-0', 'bg-sky-200' => $wakeupSound->id !== $extraWakeUp, 'bg-green-400' => $wakeupSound->id === $extraWakeUp])>
                            <div class="flex flex-col justify-center w-1/6">
                                @include('includes.components.audio-player-min', [
                                    'soundId' => "wakeup-sound-" . $wakeupSound->id,
                                    'src' => asset('storage/' . $wakeupSound->sample),
                                ])
                            </div>
                            <div class="flex flex-col justify-center w-2/3 mb-2">
                                <p class="pt-2 text-sm text-slate-800">{{ $wakeupSound->name }}</p>
                            </div>
                            <div class="flex flex-col justify-center w-1/6 my-4 lg:my-0">
                                <p>
                                    <button wire:click="chooseWakeUp({{ $wakeupSound->id }})" @class(['w-6 h-6 text-sm pt-[1px] font-semibold text-center text-white transition rounded-full bg-sky-500 hover:bg-sky-400', 'hidden' => $wakeupSound->id === $extraWakeUp])>
                                        <i class="fa-solid fa-plus"></i>
                                    </button>
                                    <button wire:click="removeWakeUp" @class(['w-6 h-6 text-sm pt-[1px] font-semibold text-center text-white transition rounded-full bg-red-400 hover:bg-red-300', 'hidden' => $wakeupSound->id !== $extraWakeUp])>
                                        <i class="fa-solid fa-xmark"></i>
                                    </button>
                                </p>
                            </div>
                        </div>
                    @empty
                        <p class="text-sm text-center"><em>Pas de réveil disponible pour le moment...</em></p>
                    @endforelse
                </div>
            </div>

            <div @class(["mb-5", 'hidden' => $ring]) wire:loading.remove>
                <h3 class="pb-4 text-lg font-medium text-center text-sky-500">
                    Choisissez un nom pour votre réveil (optionnel)
                </h3>
                <div class="w-full overflow-y-auto mb-7">
                    <x-text-input wire:model="name" class="block w-full mt-1" placeholder="sonnerie-001" />
                </div>
            </div>

            <div @class(["w-full", 'hidden' => count($selectedSounds) === 0])>
                <div @class(['hidden' => !$ring])>
                    <div class='mb-4 text-center'>
                        <button wire:click='downloadRing' class="px-5 py-2 text-white transition border rounded bg-sky-500 border-sky-500 hover:bg-green-500 hover:border-green-500">
                            Télécharger ma sonnerie
                        </button>
                    </div>
                    <div class='mb-4 text-center'>
                        <a href="{{ route('dashboard') }}" class="text-sky-500 hover:underline">
                            Voir toutes mes sonneries
                        </a>
                    </div>
                    <div class="mb-7">
                        @if ($ring)
                        @include('includes.components.audio-player', [
                            'soundId' => "result-ring",
                            'src' => asset('storage/' . $ring),
                            'withTime' => true,
                            'duration' => 180,
                        ])
                        @endif
                    </div>
                </div>

                <div @class(['hidden' => $ring])>
                    <div wire:loading.remove class="text-center">
                        <button wire:click='generateRing' class="px-5 py-2 transition border rounded border-sky-500 text-sky-500 hover:bg-sky-500 hover:text-white">
                            Générer ma sonnerie
                        </button>
                    </div>
                    <div class='w-full mx-auto pt-7' wire:loading wire:target='generateRing'>
                        <div class="mx-auto pulse my-7"></div>
                        <div class="pt-12 text-sm font-light text-center text-slate-700">
                            Création en cours...
                        </div>
                    </div>
                </div>

                <div class="mt-5 text-center" wire:loading.remove wire:target='generateRing'>
                    <button wire:click='resetSounds' class="hover:underline">
                        Réinitialiser
                    </button>
                </div>
            </div>
            @endguest
        </div>
    </div>
</div>
