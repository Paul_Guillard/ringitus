<?php

use App\Services\BrevoApi;
use Livewire\Volt\Component;

new class extends Component
{
    public bool $isRegistered = false;
    public bool $acceptsDataSharing = false;

    public function mount(): void
    {
        $this->isRegistered = auth()->user()->follow_up_agreed;
        $this->acceptsDataSharing = auth()->user()->data_publication_agreed ;
    }

    public function toggleNewsletterRegistration(): void
    {
        try {
            // Condition added to avoid tests failure
            // BrevoApi is only called when not in testing mode
            if (!app()->runningUnitTests()) {
                if (!auth()->user()->follow_up_agreed) {
                    BrevoApi::createContact(auth()->user()->email, auth()->user()->first_name, auth()->user()->last_name);
                } else {
                    BrevoApi::deleteContact(auth()->user()->email);
                }
            }
        } catch (Exception $e) {

        } finally {
            auth()->user()->follow_up_agreed = !auth()->user()->follow_up_agreed;
            auth()->user()->save();
            $this->isRegistered = !$this->isRegistered;
        }
    }

    public function toggleDataPublicationAgreement(): void
    {
        auth()->user()->data_publication_agreed = !auth()->user()->data_publication_agreed;
        auth()->user()->save();
        $this->acceptsDataSharing = !$this->acceptsDataSharing;
    }
}; ?>

<div class="py-2">
    <div class="mx-auto space-y-6 max-w-7xl lg:pr-5">
        <div class="p-4 bg-white shadow sm:p-8 sm:rounded-lg">
            <div class="flex flex-wrap justify-between">
                <p class="w-full mb-5 lg:mb-0 lg:w-4/5 text-slate-700">
                    J'accepte de recevoir des e-mails de la part de RG EIRL (newsletter, suivi, ...)
                </p>
                <div class="flex justify-center w-full lg:justify-end lg:w-1/5">
                    <div wire:loading wire:target='toggleNewsletterRegistration' class="hidden mr-5 lg:block text-slate-700">
                        Mise à jour...
                    </div>
                    <div
                        @class([
                            'relative cursor-pointer rounded-full h-6 w-12 shadow hover:shadow-lg transition',
                            'bg-slate-300' => !$isRegistered,
                            'bg-sky-500' => $isRegistered,
                        ])
                        wire:click='toggleNewsletterRegistration'
                    >
                        <div @class([
                            'bg-white absolute h-6 w-6 rounded-full top-0 transition-all duration-300 border border-slate-200',
                            'left-0' => !$isRegistered,
                            'left-6' => $isRegistered
                        ])></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="p-4 bg-white shadow sm:p-8 sm:rounded-lg">
            <div class="flex flex-wrap justify-between">
                <p class="w-full mb-5 lg:mb-0 lg:w-4/5 text-slate-700">
                    J'accepte de partager anonymement les informations liées à mes acouphènes afin qu'elles contribuent à la recherche scientifique
                </p>
                <div class="flex justify-center w-full lg:justify-end lg:w-1/5">
                    <div
                        @class([
                            'relative cursor-pointer rounded-full h-6 w-12 shadow hover:shadow-lg transition',
                            'bg-slate-300' => !$acceptsDataSharing,
                            'bg-sky-500' => $acceptsDataSharing,
                        ])
                        wire:click='toggleDataPublicationAgreement'
                    >
                        <div @class([
                            'bg-white absolute h-6 w-6 rounded-full top-0 transition-all duration-300 border border-slate-200',
                            'left-0' => !$acceptsDataSharing,
                            'left-6' => $acceptsDataSharing
                        ])></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="p-4 bg-white shadow sm:p-8 sm:rounded-lg">
            <div class="max-w-xl">
                <livewire:profile.update-profile-information-form />
            </div>
        </div>

        <div class="p-4 bg-white shadow sm:p-8 sm:rounded-lg">
            <div class="max-w-xl">
                <livewire:profile.update-password-form />
            </div>
        </div>

        <div class="p-4 bg-white shadow sm:p-8 sm:rounded-lg">
            <div class="max-w-xl">
                <livewire:profile.delete-user-form />
            </div>
        </div>
    </div>
</div>

