<?php

use App\Livewire\Actions\Logout;
use App\Services\BrevoApi;
use Illuminate\Support\Facades\Auth;
use Livewire\Volt\Component;

new class extends Component
{
    public string $password = '';

    /**
     * Delete the currently authenticated user.
     */
    public function deleteUser(Logout $logout): void
    {
        $this->validate([
            'password' => ['required', 'string', 'current_password'],
        ]);

        auth()->user()->questions()->detach();

        foreach (auth()->user()->rings as $ring) {
            $ring->user_id = null;
            $ring->save();
        }

        try {
            BrevoApi::deleteContact(auth()->user()->email);
        } catch (\Throwable $th) {

        }

        tap(Auth::user(), $logout(...))->delete();

        $this->redirect('/', navigate: true);
    }
}; ?>

<section class="space-y-6">
    <header>
        <h2 class="text-lg font-medium text-slate-700">
            Supprimer votre compte
        </h2>

        <p class="mt-1 text-sm text-slate-700">
            La suppression de votre compte entrainera la suppression de toutes les données qui y sont associées, de manière définitive. Vous serez également effacé des destinataires de notre mailing list.
        </p>
    </header>

    <x-danger-button
        x-data=""
        x-on:click.prevent="$dispatch('open-modal', 'confirm-user-deletion')"
    >Supprimer votre compte</x-danger-button>

    <x-modal name="confirm-user-deletion" :show="$errors->isNotEmpty()" focusable>
        <form wire:submit="deleteUser" class="p-6">

            <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
                Êtes-vous sûr.e de vouloir supprimer votre compte ? Cette action est définitive.
            </h2>

            <p class="mt-1 text-sm text-gray-600 dark:text-gray-400">
                Une fois votre compte supprimé, toutes vos données personnelles seront effacées. Entrez votre mot de passe pour confirmer la suppression.
            </p>

            <div class="mt-6">
                <x-input-label for="password" value="Mot de passe" class="sr-only" />

                <x-text-input
                    wire:model="password"
                    id="password"
                    name="password"
                    type="password"
                    class="block w-3/4 mt-1"
                    placeholder="Mot de passe"
                />

                <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>

            <div class="flex justify-end mt-6">
                <x-secondary-button x-on:click="$dispatch('close')">
                    Annuler
                </x-secondary-button>

                <x-danger-button class="ml-3">
                    Supprimer le compte
                </x-danger-button>
            </div>
        </form>
    </x-modal>
</section>
