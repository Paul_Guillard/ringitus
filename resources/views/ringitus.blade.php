@extends('layouts.app')

@section('content')
    <section class="relative z-10 w-full h-[100svh] lg:h-[120vh] lg:overflow-hidden">
        <img src="{{ asset('assets/pictures/Wave-33.3s-2023px.svg') }}" alt="Photo bannière Ringitus" class="absolute left-0 object-cover w-full rotate-180 -z-10 bottom-0 lg:bottom-24 lg:top-64 scale-x-[-1]">
        <div class="absolute bottom-0 left-0 w-full h-16 lg:h-32 bg-gradient-to-b from-transparent to-white"></div>
        <div class="container pl-24 text-center pt-36 lg:pt-56">
            <h1 class="mb-10 text-3xl font-bold lg:text-5xl text-sky-500">
                Un réveil matin pour <span class="text-green-400">réduire les acouphènes</span>
            </h1>
            <p class="mb-0 text-base lg:mb-12 lg:text-xl text-slate-700">
                <span class="animate__animated animate__fadeIn animate__slower">Sélectionnez des échantillons sonores.</span> <span class="inline lg:hidden"><br/></span> <span class="animate__animated animate__fadeIn animate__slower animate__delay-1s">Fabriquez la combinaison qui soulage vos acouphènes.</span> <span class="inline lg:hidden"><br/></span><span class="animate__animated animate__fadeIn animate__slower animate__delay-2s">Transformez-la en réveil-matin.</span>
            </p>
            <div class="flex flex-wrap justify-center">
                <div class="mx-5 my-7">
                    <a href="#sound-generator" x-on:click.prevent="$scrollTo({ offsetHeader: true })" class="inline-block px-5 py-3 text-lg transition duration-500 bg-white rounded-lg shadow-lg w-60 text-slate-700 hover:bg-sky-500 hover:text-white">
                        Créez votre réveil
                    </a>
                </div>
                {{-- <div class="mx-5 my-7">
                    <button class="px-5 py-3 text-lg transition duration-500 bg-white rounded-lg shadow-lg w-60 text-slate-700 hover:bg-sky-500 hover:text-white">
                        Comment ça fonctionne
                    </button>
                </div> --}}
            </div>
        </div>
    </section>

    <section id="sound-generator">
        @include('includes.sections.sound-generator')
    </section>
@endsection
