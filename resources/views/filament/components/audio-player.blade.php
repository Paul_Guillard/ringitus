<div class="w-full">
    <audio controls class="w-full">
        <source src="{{ asset('storage/' . $getRecord()->sample) }}" type="audio/mpeg">
        The sound track cannot be played on this browser...
    </audio>
</div>