<div class="relative flex flex-wrap justify-between lg:flex-nowrap">
    <div id="selection-btn" @class(['block lg:hidden fixed bottom-6 border border-sky-500 right-2 bg-white text-center mx-auto p-3 text-sky-500 rounded-lg cursor-pointer opacity-0 -z-10 shadow'])>
        <a href="#selection" class="inline-block w-full h-full">
            Voir ma sélection
        </a>
    </div>
    <div class="container w-full lg:w-3/4 lg:pr-7">
        @include('includes.components.sound-list', [
            'title' => 'Nos suggestions',
            'sounds' => collect([]),
            'theme' => 'suggested',
        ])

        @include('includes.components.sound-list', [
            'title' => 'Sons de la nature',
            'sounds' => collect([]),
            'theme' => 'nature',
        ])

        @include('includes.components.sound-list', [
            'title' => 'Bruits blancs',
            'sounds' => collect([]),
            'theme' => 'wave',
        ])
    </div>

    <div class="w-full lg:w-1/4 lg:px-7 min-w-[270px]" id="selection">
        <div class="sticky w-full border top-20 border-slate-200 rounded-xl p-7 lg:max-h-[85vh] overflow-y-auto">
            <span class="font-light text-sky-500">Chargement en cours...</span>
        </div>
    </div>
</div>
