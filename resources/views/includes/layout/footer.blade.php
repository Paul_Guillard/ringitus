<footer class="w-full py-10 mt-12 font-light bg-white border-t border-slate-200 text-slate-700">
    <div class="container flex flex-col-reverse flex-wrap justify-around mb-10 lg:flex-row">
        <div class="w-full lg:w-auto">
            <img src="{{ asset('assets/pictures/logo_ringitus.png') }}" alt="Logo Ringitus" class="h-16 mx-auto">
        </div>
        <ul class="w-full px-10 lg:px-0 mb-7 lg:w-auto lg:mb-0">
            <li class="my-1 transition lg:my-0 hover:text-sky-500"><a href="{{ route('legal-mentions') }}">Mentions légales</a></li>
            <li class="my-1 transition lg:my-0 hover:text-sky-500"><a href="{{ route('general-conditions') }}">Conditions Générales d'Utilisation</a></li>
            <li class="my-1 transition lg:my-0 hover:text-sky-500"><a href="{{ route('data-policy') }}">Politique de confidentialité</a></li>
            {{-- <li class="transition hover:text-sky-500"><a href="#">À propos de nous</a></li> --}}
            <li class="my-1 transition lg:my-0 hover:text-sky-500"><a href="{{ route('contact') }}">Nous contacter</a></li>
        </ul>
    </div>
    <p class="text-sm text-center">
        <i class="mr-2 fa-regular fa-copyright"></i> Copyright 2023
    </p>
</footer>