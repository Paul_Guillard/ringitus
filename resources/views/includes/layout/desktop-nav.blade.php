<header class="fixed z-20 hidden w-full h-20 lg:block" x-scroll-to-header>
    <nav class="flex justify-between w-full h-full font-light main-nav text-slate-700">
        <div class="flex justify-start">
            @if (Route::currentRouteName() === 'home')
            <a href="#hero" x-on:click.prevent="$scrollTo({ offsetHeader: true })" class="block p-6 text-lg text-center">
                <img src="{{ asset('assets/pictures/logo_ringitus.png') }}" alt="Logo Ringitus" class="h-10">
            </a>
            @else
            <a href="{{ route('home') }}" class="block p-6 text-lg text-center">
                <img src="{{ asset('assets/pictures/logo_ringitus.png') }}" alt="Logo RG EIRL" class="h-10">
            </a>
            @endif
        </div>
        <ul class="flex justify-end text-lg text-slate-700">
            @auth
            @if (Route::CurrentRouteName() === 'dashboard')
                <li class="h-full bg-opacity-60 w-fit">
                    <a href="{{ route('ringitus') . '#sound-generator' }}" class="flex flex-col justify-center h-full px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                        <p><i class="mr-2 fa-solid fa-bell"></i> Créer une sonnerie</p>
                    </a>
                </li>
            @endif
            <li class="h-full bg-opacity-60 w-fit">
                <a href="{{ route('dashboard') }}" class="flex flex-col justify-center h-full px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                    <p><i class="mr-2 fa-solid fa-circle-user"></i> Mon tableau de bord</p>
                </a>
            </li>
            @else
            <li class="flex flex-col justify-center h-full bg-opacity-60">
                <a href="{{ route('login') }}" class="px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                    <i class="mr-2 text-sm fa-solid fa-right-to-bracket text-sky-500"></i>Se connecter
                </a>
            </li>
            <li class="flex flex-col justify-center h-full bg-opacity-60">
                <a href="{{ route('register') }}" class="block px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                    <i class="mr-2 text-sm fa-solid fa-user-plus text-sky-500"></i>Créer un profil
                </a>
            </li>
            @endauth
        </ul>
    </nav>
</header>
