<div class="fixed top-0 z-50 w-full h-full pt-16 transition-all duration-500 bg-white" :class="mobileMenu ? 'left-0' : '-left-[100vw]'">
    <ul>
        @auth
        @if (Route::CurrentRouteName() === 'dashboard')
            <li class="h-16 border-b bg-opacity-60 border-slate-200">
                <a href="{{ route('ringitus') . '#sound-generator' }}" class="flex flex-col justify-center h-full px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                    <p><i class="mr-2 fa-solid fa-bell"></i> Créer une sonnerie</p>
                </a>
            </li>
        @else
            <li class="h-16 border-b bg-opacity-60 border-slate-200">
                <a href="{{ route('dashboard') }}" class="flex flex-col justify-center h-full px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                    <p><i class="mr-2 fa-solid fa-circle-user"></i> Mon tableau de bord</p>
                </a>
            </li>
        @endif
        <li class="h-16 border-b bg-opacity-60 border-slate-200">
            <form action="{{ route('logout') }}" method="POST" class="w-full h-full">
                @csrf
                <div class="flex flex-col justify-center w-full h-full">
                    <button type="submit" class="inline-block w-full h-full px-10 text-left transition duration-500 ease-out main-nav__link hover:text-sky-500">
                        <i class="mr-2 fa-solid fa-right-from-bracket"></i> Se déconnecter
                    </button>
                </div>
            </form>
        </li>
        @else
        <li class="h-16 border-b bg-opacity-60 border-slate-200">
            <a href="{{ route('login') }}" class="flex flex-col justify-center h-full px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                <p><i class="mr-2 text-sm fa-solid fa-right-to-bracket"></i>Se connecter</p>
            </a>
        </li>
        <li class="h-16 border-b bg-opacity-60 border-slate-200">
            <a href="{{ route('register') }}" class="flex flex-col justify-center h-full px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                <p><i class="mr-2 text-sm fa-solid fa-user-plus"></i>Créer un profil</p>
            </a>
        </li>
        @endauth
        <li class="h-16 border-b bg-opacity-60 border-slate-200">
            <a href="{{ route('contact') }}" class="flex flex-col justify-center h-full px-10 transition duration-500 ease-out main-nav__link hover:text-sky-500">
                <p><i class="mr-2 text-sm fa-solid fa-envelope"></i>Nous contacter</p>
            </a>
        </li>
    </ul>
</div>
<div @click="mobileMenu = false" class="fixed top-0 left-0 w-full h-full transition-all duration-300 opacity-0 bg-slate-700" :class="mobileMenu ? 'z-40 opacity-90' : '-z-10 opacity-0'"></div>
