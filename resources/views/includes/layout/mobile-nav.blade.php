<header class="fixed z-50 block w-full h-16 lg:hidden" x-scroll-to-header>
    <nav class="flex justify-between w-full h-full font-light main-nav text-slate-700">
        <div class="flex justify-start">
            @if (Route::currentRouteName() === 'home')
            <a href="#hero" x-on:click.prevent="$scrollTo({ offsetHeader: true })" class="block p-4 text-lg text-center">
                <img src="{{ asset('assets/pictures/logo_ringitus.png') }}" alt="Logo Ringitus" class="h-10">
            </a>
            @else
            <a href="{{ route('home') }}" class="block p-4 text-lg text-center">
                <img src="{{ asset('assets/pictures/logo_ringitus.png') }}" alt="Logo RG EIRL" class="h-10">
            </a>
            @endif
        </div>
        <div x-show="!mobileMenu" @click="mobileMenu = true" class="p-4 text-2xl transition cursor-pointer pr-7 text-slate-700 hover:text-sky-500">
            <i class="fa-solid fa-bars"></i>
        </div>
        <div x-show="mobileMenu" @click="mobileMenu = false" class="p-4 text-2xl transition cursor-pointer pr-7 text-slate-700 hover:text-sky-500">
            <i class="fa-solid fa-xmark"></i>
        </div>
    </nav>
</header>