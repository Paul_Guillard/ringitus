<div class="mb-20">
    @if ($theme === 'nature')
        <div class="relative py-5 mb-10 px-7 bg-gradient-to-r to-sky-500 from-green-400 rounded-xl">
    @elseif ($theme === 'suggested')
        <div class="relative py-5 mb-10 px-7 bg-gradient-to-r to-sky-500 from-yellow-400 rounded-xl">
    @else
        <div class="relative py-5 mb-10 px-7 bg-gradient-to-r to-sky-400 from-slate-400 rounded-xl">
    @endif
        <h2 class="w-full text-2xl font-semibold text-white">
            {{ $title }}
        </h2>
        @if ($theme === 'nature')
        <p class="absolute text-white -top-4 right-1 text-9xl opacity-40">
            <i class="fa-solid fa-leaf"></i>
        </p>
        @elseif ($theme === 'suggested')
        <p class="absolute text-white text-7xl -top-1 right-1 opacity-40">
            <i class="fa-solid fa-star"></i>
        </p>
        @else
        <p class="absolute top-0 text-6xl text-white right-1 opacity-40">
            <i class="fa-solid fa-ear-listen"></i>
        </p>
        @endif
    </div>
    <div class="max-h-[400px] overflow-y-auto">
    @foreach ($sounds as $sound)
        <div @class(['flex flex-wrap justify-center w-full py-2 mb-4 rounded-xl px-7', 'bg-sky-200' => !isset($selectedSounds[$sound->id]), 'bg-green-500' => isset($selectedSounds[$sound->id])]) wire:key="sound-{{ $sound->id }}">
            <div class="flex flex-col justify-center w-full mb-4 overflow-hidden lg:pr-2 lg:w-1/3 lg:mb-0">
                <p class="text-lg text-center lg:text-left text-slate-700">
                    {{ ucfirst($sound->name) }}
                </p>
            </div>
            <div class="w-5/6 lg:w-1/2">
                @include('includes.components.audio-player', [
                    'soundId' => "sounds-list-" . $sound->id,
                    'src' => asset('storage/' . $sound->sample),
                ])
            </div>
            <div class="flex flex-col justify-start w-1/6 my-4 lg:justify-center lg:my-0">
                <p class="pl-3 mr-0 text-center lg:pl-0 lg:text-right">
                    <button wire:click="addToSelection({{ $sound->id }})" class="w-8 h-8 text-2xl font-semibold text-center text-white transition rounded-full bg-sky-500 hover:bg-sky-400">
                        +
                    </button>
                </p>
            </div>
        </div>
    @endforeach
    </div>
    @if (isset($credits))
        <p class="py-2 text-sm text-right">
            <em>{!! $credits !!}</em>
        </p>
    @endif
</div>
