<div class="flex flex-wrap justify-center w-full" x-data="{ playing: false }">
    <audio controls class="hidden w-full lg:pl-2" id="{{ $soundId }}" preload="metadata">
        <source src="{{ $src }}" type="audio/mpeg">
        Nous n'avons pas pu lire le fichier son sur votre navigateur...
    </audio>
    <div>
        <div
            x-show="!playing"
            class="text-center transition cursor-pointer text-sky-500 hover:text-green-400"
            @click="document.getElementById(@js($soundId )).play(); playing = true;"
        >
            <i class="text-xl fa-solid fa-play"></i>
        </div>
        <div
            x-show="playing"
            class="text-center transition cursor-pointer text-sky-500 hover:text-green-400"
            @click="document.getElementById(@js($soundId)).pause(); playing = false;"
        >
            <i class="text-xl fa-solid fa-pause"></i>
        </div>
    </div>
</div>