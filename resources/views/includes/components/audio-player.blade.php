<div class="flex flex-wrap justify-center w-full" x-data="{ playing: false }">
    <audio controls class="hidden w-full lg:pl-2" id="{{ $soundId }}" preload="metadata">
        <source src="{{ $src }}" type="audio/mpeg">
        Nous n'avons pas pu lire le fichier son sur votre navigateur...
    </audio>
    <div @class(['w-fit', 'mr-2' => !(isset($withTime) && $withTime)])>
        <div
            x-show="!playing"
            class="w-12 h-12 pt-1 pl-1 text-center transition bg-white border-2 rounded-full cursor-pointer border-sky-500 text-sky-500 hover:shadow-lg hover:text-green-400 hover:border-green-400"
            @click="document.getElementById(@js($soundId )).play(); playing = true;"
        >
            <i class="text-3xl fa-solid fa-play"></i>
        </div>
        <div
            x-show="playing"
            class="w-12 h-12 pt-1 text-center transition bg-white border-2 rounded-full cursor-pointer border-sky-500 text-sky-500 hover:shadow-lg hover:text-green-400 hover:border-green-400"
            @click="document.getElementById(@js($soundId)).pause(); playing = false;"
        >
            <i class="text-3xl fa-solid fa-pause"></i>
        </div>
    </div>
    @if (!isset($withTime) || (isset($withTime) && $withTime === false))
    <div class="w-fit">
        <div
            class="w-8 h-8 pt-1 mt-2 text-center transition bg-white border-2 rounded-full cursor-pointer border-sky-500 text-sky-500 hover:shadow-lg hover:text-green-400 hover:border-green-400"
            @click="document.getElementById(@js($soundId)).currentTime = 0"
        >
            <i class="fa-solid fa-arrow-rotate-left"></i>
        </div>
    </div>
    @endif
    <div
        @class(['flex min-w-[50px] min-h-[50px]', 'w-full lg:w-1/2 mx-2' => (isset($withTime) && $withTime) , 'w-full lg:w-7/12 mx-2' => (!isset($withTime) || (isset($withTime) && $withTime === false))])
        x-data="{ volume: 1 }"
        x-init="$watch('volume', value => document.getElementById(@js($soundId)).volume = value)"
    >
        <input class="w-5/6 lg:w-4/5" type="range" min="0" max="1" step="0.1" x-model="volume">
        <div class="flex flex-col justify-center w-1/6 ml-2 cursor-pointer lg:-1/5 text-sky-500">
            <i x-show="volume > 0.5" class="fa-solid fa-volume-high" @click="volume = 0"></i>
            <i x-show="volume <= 0.5 && volume > 0" class="fa-solid fa-volume-low" @click="volume = 0"></i>
            <i x-show="volume == 0" class="fa-solid fa-volume-off" @click="volume = 1"></i>
        </div>
    </div>
    @if (isset($withTime) && $withTime)
    <div
        class='w-full pt-2'
        x-data="{ time: 0, displayed: 0 }"
        x-init="
            setInterval(function() {
                time = document.getElementById(@js($soundId)).currentTime
                displayed = time
            }, 2000);
        "
    >
        <input class="w-full" type="range" min="0" x-bind:max="@js($duration ?? 60)" step="1" x-model="time" @change="document.getElementById(@js($soundId)).currentTime = time; displayed = time;">
        <p class="text-sm text-center">
            <em><span x-text="(displayed-(displayed%=60))/60+(9<displayed?':':':0') + Math.floor(displayed)"></span></em>
        </p>
    </div>
    @endif
</div>