<div class="container relative z-10 -mt-6 lg:-mt-24">
    <div class="bg-white border p-7 border-slate-200 rounded-xl">
        <h2 class="py-10 text-2xl font-semibold text-center lg:text-4xl text-sky-500">
            Créez votre réveil matin
        </h2>

        @guest
        <div class="container flex flex-wrap justify-center lg:justify-start mb-7 text-slate-700">
            <div class="w-full mb-4 lg:w-1/12 lg:mb-0">
                <div class="flex flex-col justify-center w-12 h-12 mx-auto text-center rounded-full bg-sky-500">
                    <p class="text-xl font-semibold text-white">1</p>
                </div>
            </div>
            <p class="w-full pt-0 text-lg font-light text-center lg:text-left lg:pl-5 lg:w-11/12">
                <a href="{{ route('register') }}" class="text-sky-600">Créez un compte</a> sur notre plate-forme ou <a href="{{ route('login') }}" class="text-sky-600">connectez-vous</a> à votre compte pour pouvoir sélectionner des pistes sonores.
            </p>
        </div>
        @endguest
        <div class="container flex flex-wrap justify-center lg:justify-start mb-7 text-slate-700">
            <div class="w-full mb-4 lg:w-1/12 lg:mb-0">
                <div class="flex flex-col justify-center w-12 h-12 mx-auto text-center rounded-full bg-sky-500">
                    <p class="text-xl font-semibold text-white">@guest 2 @else 1 @endguest</p>
                </div>
            </div>
            <p class="w-full pt-0 text-lg font-light text-center lg:text-left lg:pl-5 lg:w-11/12">
                Écoutez les échantillons sonores dans la liste ci-dessous, et sélectionnez ceux qui vous conviennent. Utilisez le bouton
                <span class="inline-block w-8 h-8 text-2xl font-semibold text-center text-white transition rounded-full min-w-12 bg-sky-500 hover:bg-sky-400">
                    +
                </span> pour ajouter ces échantillons sonores à votre sélection.
            </p>
        </div>
        <div class="container flex flex-wrap justify-center lg:justify-start mb-7 text-slate-700">
            <div class="w-full mb-4 lg:w-1/12 lg:mb-0">
                <div class="flex flex-col justify-center w-12 h-12 mx-auto text-center rounded-full bg-sky-500">
                    <p class="text-xl font-semibold text-white">@guest 3 @else 2 @endguest</p>
                </div>
            </div>
            <p class="w-full pt-0 text-lg font-light text-center lg:text-left lg:pl-5 lg:w-11/12">
                Si nécessaire, ajoutez une sonnerie de réveil matin 'classique', qui assurera votre sortie du sommeil. Cette étape est optionnelle.
            </p>
        </div>
        <div class="container flex flex-wrap justify-center lg:justify-start mb-7 text-slate-700">
            <div class="w-full mb-4 lg:w-1/12 lg:mb-0">
                <div class="flex flex-col justify-center w-12 h-12 mx-auto text-center rounded-full bg-sky-500">
                    <p class="text-xl font-semibold text-white">@guest 4 @else 3 @endguest</p>
                </div>
            </div>
            <p class="w-full pt-0 text-lg font-light text-center lg:text-left lg:pl-5 lg:w-11/12">
                Appuyez sur 'Générer ma sonnerie', et patientez quelques instants ! En fonction de votre sélection, un réveil matin sera généré et disponible à l'écoute et au téléchargement. Et voilà !
            </p>
        </div>

        <div class="flex justify-between w-full p-4 mx-auto my-5 text-red-700 bg-red-100 rounded-xl lg:p-7 lg:w-3/4">
            <div class="flex flex-col justify-center pr-4 text-3xl text-center lg:text-4xl w-fit">
                <i class="fa-solid fa-triangle-exclamation"></i>
            </div>
            <div class="pl-5 w-fit">
                <p>
                    <strong>Attention !</strong> Avant de procéder à l'écoute des échantillons sonores,
                    assurez-vous d'ajuster correctement le volume de votre appareil, afin de profiter au mieux de
                    l'expérience tout en protégeant votre audition.
                </p>
            </div>
        </div>

        <div x-data="{ open: false }" class="my-12">
            <h3 class="my-2 text-xl text-center cursor-pointer text-sky-500" @click="open = !open">
                Besoin d'aide ? Afficher les vidéos de démonstration <i class="ml-2 transition fa-solid fa-chevron-right" :class="open ? 'rotate-90' : 'rotate-0'"></i>
            </h3>
            <div x-show="open" x-transition.opacity>
                <div class="container my-12">
                    <iframe loading="lazy" class="w-full shadow-xl lg:w-4/5 aspect-video" src="https://www.youtube.com/embed/3ZWehoDvaew" style="border-radius: 8px; margin: auto;" title="Présentation et tutoriel pour créer votre piste sonore !" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>

                <div class="container my-12">
                    <iframe loading="lazy" class="w-full shadow-xl lg:w-4/5 aspect-video" src="https://www.youtube.com/embed/M1AZkPx3sVY" style="border-radius: 8px; margin: auto;" title="Tutoriel d&#39;installation de votre sonnerie sur Android !" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>

                <div class="container my-12">
                    <iframe loading="lazy" class="w-full shadow-xl lg:w-4/5 aspect-video" src="https://www.youtube.com/embed/HdkJZFoXBpE" style="border-radius: 8px; margin: auto;" title="Tutoriel d&#39;installation de votre sonnerie sur Iphone !" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        <livewire:sounds.sound-mixer lazy="on-load">
    </div>
</div>
