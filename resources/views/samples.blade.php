@extends('layouts.app')

@section('content')
<div class="pt-20">
    <h1 class="py-10 text-4xl font-semibold text-center text-amber-600 bm-12">
        Notre liste de sons pour soulager vos acouphènes
    </h1>

    @livewire('sounds.sound-mixer')
</div>
@endsection