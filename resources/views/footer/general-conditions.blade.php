@extends('layouts.app')

@section('title')
    Conditions Générales d'Utilisation
@endsection

@section('content')
    <div class="container pt-28">
        <h1 class='mb-4 text-3xl font-medium text-center text-sky-500'>Conditions générales d'utilisation</h1>
        <p class="mb-12 italic text-center">Dernière mise à jour : 06 décembre 2023</p>

        <p class="mb-2">
            Les présentes conditions générales d’utilisation (dites « CGU ») ont pour objet l’encadrement juridique des modalités de mise à disposition du site et des services par Robin Guillard EIRL (RG EIRL) et de définir les conditions d’accès et d’utilisation des services par « l’Utilisateur ».
        </p>
        <p class="mb-12">
            Les présentes CGU sont accessibles sur le site à la rubrique 'Conditions Générales d'utilisation'.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            1. Accès au site
        </h2>
        <p class="mb-2">
            Le site <a href="{{ route('home') }}">{{ route('home') }}</a> et les services associés permettent à l’Utilisateur un accès gratuit aux services suivants :
        </p>
        <p class="mb-2">
            L'aide et la vulgarisation scientifique à destination des personnes souffrant de troubles auditifs (acouphènes, hyperacousie, malentendance…) et la contribution à la recherche scientifique du domaine des troubles auditifs.
        </p>
        <p class="mb-12">
            Le site et les services associés sont accessibles gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l’Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            2. Propriété intellectuelle
        </h2>
        <p class="mb-2">
            Les marques, logos, signes ainsi que tous les contenus du site et de l’application mobile (textes, images, son…) font l’objet d’une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d’auteur.
        </p>
        <p class="mb-2">
            L’Utilisateur doit solliciter l’autorisation préalable du site et des services associés pour toute reproduction, publication, copie des différents contenus. Il s’engage à une utilisation des contenus du site et/ou des services associés dans un cadre strictement privé, toute utilisation à des fins commerciales et publicitaires est strictement interdite.
        </p>
        <p class="mb-2">
            Toute représentation totale ou partielle de ce site et/ou des services associés par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet  et des services associés constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle.
        </p>
        <p class="mb-12">
            Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’Utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            3. Responsabilité
        </h2>
        <p class="mb-2">
            Les sources des informations diffusées sur le site <a href="{{ route('home') }}">{{ route('home') }}</a>  et des services associés sont réputées fiables mais le site  et des services associés ne garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.
        </p>
        <p class="mb-2">
            Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, le site <a href="{{ route('home') }}">{{ route('home') }}</a> et des services associés ne peuvent être tenus responsable de la modification des dispositions administratives et juridiques survenant après la publication. De même, le site et l’application ne peuvent être tenus responsables de l’utilisation et de l’interprétation de l’information contenue dans ce site et des services associé.
        </p>
        <p class="mb-2">
            Le site <a href="{{ route('home') }}">{{ route('home') }}</a>  et des services associés ne peuvent être tenu pour responsables d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site ou cette application.
        </p>
        <p class="mb-12">
            La responsabilité du site  et des services associés ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d’un tiers.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            4. Liens hypertexte
        </h2>
        <p class="mb-12">
            Des liens hypertextes peuvent être présents sur le site et des services associés. L’Utilisateur est informé qu’en cliquant sur ces liens, il sortira du site <a href="{{ route('home') }}">{{ route('home') }}</a> et/ou des services associés. Ce dernier n’a pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne saurait, en aucun cas, être responsable de leur contenu.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            5. Publications par l’Utilisateur
        </h2>
        <p class="mb-2">
            Le site et des services associés permettent aux membres de publier les contenus suivants :
        </p>
        <p class="mb-2">
            Témoignages sur les prises en charges essayées sur les acouphènes, commentaires sur les contenus publiés
        </p>
        <p class="mb-2">
            Dans ses publications, le membre s’engage à respecter les règles de la Netiquette (règles de bonne conduite de l’internet) et les règles de droit en vigueur.
        </p>
        <p class="mb-2">
            Le site et/ou les services associés peuvent exercer une modération sur les publications et se réserve le droit de refuser leur mise en ligne, sans avoir à s’en justifier auprès du membre.
        </p>
        <p class="mb-2">
            Le membre reste titulaire de l’intégralité de ses droits de propriété intellectuelle. Mais en publiant une publication sur le site et/ou sur l’application mobile, il cède à la société éditrice le droit non exclusif et gratuit de représenter, reproduire, adapter, modifier, diffuser et distribuer sa publication, directement ou par un tiers autorisé, dans le monde entier, sur tout support (numérique ou physique), pour la durée de la propriété intellectuelle. Le Membre cède notamment le droit d’utiliser sa publication sur internet et sur les réseaux de téléphonie mobile.
        </p>
        <p class="mb-2">
            La société éditrice s’engage à faire figurer le nom/pseudonyme du membre à proximité de chaque utilisation de sa publication.
        </p>
        <p class="mb-2">
            Tout contenu mis en ligne par l’Utilisateur est de sa seule responsabilité. L’Utilisateur s’engage à ne pas mettre en ligne de contenus pouvant porter atteinte aux intérêts de tierces personnes. Tout recours en justice engagé par un tiers lésé contre le site  et des services associés sera pris en charge par l’Utilisateur.
        </p>
        <p class="mb-12">
            Le contenu de l’Utilisateur peut être à tout moment et pour n’importe quelle raison supprimé ou modifié par le site et/ou des services associés , sans préavis.
        </p>
    </div>
@endsection
