@extends('layouts.app')

@section('title')
    Nous contacter
@endsection

@section('content')
    <div class="container pt-28">
        <h1 class='mb-12 text-3xl font-medium text-center text-sky-500'>
            Nous contacter
        </h1>

        @livewire('layout.contact')
    </div>
@endsection
