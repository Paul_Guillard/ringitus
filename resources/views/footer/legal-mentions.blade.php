@extends('layouts.app')

@section('title')
    Mentions Légales
@endsection

@section('content')
<div class="container pt-28 min-h-[90vh]">
    <h1 class='mb-4 text-3xl font-medium text-center text-sky-500'>Mentions légales</h1>
    <p class="mb-12 italic text-center">Dernière mise à jour : 06 décembre 2023</p>

    <p class="mb-2">
        L’édition du site <a href="{{ route('home') }}" class="text-sky-500">{{ route('home') }}</a> et des services associés est assurée par l'entreprise individuelle à responsabilité limitée Robin Guillard EIRL, immatriculée au RCS de Grenoble sous le numéro 883028714, dont le siège social est situé au 93 cours Berriat, 38000, Grenoble
    </p>
    <p class="mb-2">
        Adresse e-mail : <a href="mailto:contact@rg-eirl.fr" class="text-sky-500">contact@rg-eirl.fr</a>
    </p>
    <p class="mb-2">
        Directeur de la publication : Robin GUILLARD
    </p>
    <p class="mb-2">
        Hébergement du site :
    </p>
    <ul class="pl-4 mb-12">
        <li><a href="{{ route('home') }}" class="text-sky-500">{{ route('home') }}</a></li>
        <li>Hébergeur : OVH</li>
        <li>Siège social : 2 rue Kellermann, 59100 Roubaix, France</li>
        <li>Numéro de téléphone : 09 72 10 10 07</li>
        <li>Hébergeur certifié HDS - Source :
            <a class="text-sky-500 hover:underline" target="_blank" href="https://esante.gouv.fr/offres-services/hds/liste-des-herbergeurs-certifies">
                Ministère de la Santé et de la Prévention<i class="ml-2 text-sm fa-solid fa-arrow-up-right-from-square"></i>
            </a>
        </li>
    </ul>

    <h2 class="mb-5 text-xl font-medium">
        Droit applicable et juridiction compétente
    </h2>
    <p class="mb-2">
        La législation française s’applique au présent contrat. En cas d’absence de résolution amiable d’un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.
    </p>
    <p class="mb-2">
        Pour toute question relative à l’application des présentes CGU, vous pouvez joindre l’éditeur aux coordonnées inscrites dans les <a href="{{ route('legal-mentions') }}" class="text-sky-500">mentions légales</a>.
    </p>
    <p class="mb-2">
        CGU réalisées à partir du modèle de <a href="http://legalplace.fr/" target="_blank" class="text-sky-500">http://legalplace.fr/<i class="ml-2 text-sm fa-solid fa-arrow-up-right-from-square"></i></a>
    </p>
</div>
@endsection
