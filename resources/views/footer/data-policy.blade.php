@extends('layouts.app')

@section('title')
    Politique de confidentialité
@endsection

@section('content')
    <div class="container pt-28">
        <h1 class='mb-4 text-3xl font-medium text-center text-sky-500'>Protection des données et politique de confidentialité</h1>
        <p class="mb-12 italic text-center">Dernière mise à jour : 06 décembre 2023</p>

        <h2 class="mb-5 text-xl font-medium">
            1. Collecte des données
        </h2>
        <p class="mb-2">
            Le site  et les services associés assurent à l’Utilisateur une collecte et un traitement d’informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés. Le site et l’application ont déclarés à la CNIL un délégué à la protection des données (Fleepit Digital, 23, rue du Peintre Lebrun, 78000 Versailles.) sous le numéro DPO-98483.
        </p>
        <p class="mb-2">
            En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l’Utilisateur dispose d’un droit d’accès, de rectification, de suppression et d’opposition de ses données personnelles. L’Utilisateur peut exercer ce droit :
        </p>
        <p class="pl-4 mb-2">
            > depuis son <a href="{{ route('dashboard') }}">espace personnel</a>, depuis lequel il peut modifier ou supprimer l'ensemble de ses données personnelles
        </p>
        <p class="pl-4 mb-12">
            > par mail à l’adresse email <a href="mailto:dpo_siopi@rgpdbox.com">dpo_siopi@rgpdbox.com</a>
        </p>

        <h2 class="mb-5 text-xl font-medium">
            2. Cookies
        </h2>
        <p class="mb-2">
            L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement sur son logiciel de navigation. Pour le moment, seuls des cookies essentiels au bon fonctionnement du site sont déposés sur l'appareil de l'Utilisateur.
        </p>
        <p class="mb-2">
            Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de l’Utilisateur par votre navigateur et qui sont nécessaires à l’utilisation du site <a href="{{ route('home') }}" class="text-sky-500">{{ route('home') }}</a>. Les cookies ne contiennent pas d’information personnelle et ne peuvent pas être utilisés pour identifier quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement et donc anonyme. Certains cookies expirent à la fin de la visite de l’Utilisateur, d’autres restent.
        </p>
        <p class="mb-2">
            L’information contenue dans les cookies est utilisée pour permettre un fonctionnement normal du site <a href="{{ route('home') }}" class="text-sky-500">{{ route('home') }}</a>.
        </p>
        <p class="mb-2">
            En navigant sur le site, l’Utilisateur les accepte par défaut. En cas d'utilisation par le site de cookies optionnels, l'Utilisateur en sera informé et aura la possibilité de les supprimer. Ces cookies ne seront pas déposés par défaut sur l'appareil de l'Utilisateur.
        </p>
        <p class="mb-12">
            L’Utilisateur pourra désactiver et supprimer tous les cookies par l’intermédiaire des paramètres figurant au sein de son logiciel de navigation.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            3. Identité et coordonnées du responsable des traitements
        </h2>
        <p class="mb-12">
            Le responsable du traitement des données est Robin Guillard EIRL (EIRL), RCS : 883028714 Grenoble, 93 cours Berriat, 38000 Grenoble, ci-après nommée « RG-EIRL ».
        </p>

        <h2 class="mb-5 text-xl font-medium">
            4. Coordonnées du Délégué à la protection des données
        </h2>
        <p class="mb-12">
            RG-EIRL a désigné un Délégué à la protection des données dont les coordonnées sont : Fleepit Digital, 23, rue du Peintre Lebrun, 78000 Versailles.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            5. Finalité des traitements mis en oeuvre
        </h2>
        <p class="mb-2">
            Le traitement a pour premier objet la promotion de l’entraide au sein de la communauté des personnes atteintes de troubles auditifs (acouphènes, hyperacousie, malentendance). Le traitement a comme second objet de contribuer à la recherche épidémiologique et clinique sur les troubles auditifs.
        </p>
        <p class="mb-12">
            Il permet de favoriser des mises en contacts entre patients sur base de similarité de leurs profils symptômes.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            6. Catégorie de Données
        </h2>
        <p class="mb-2">
            Les personnes concernées sont personnes atteintes de troubles auditifs (acouphènes, hyperacousie, malentendance…) et les associations représentantes de ces personnes.
        </p>
        <p class="mb-2">
            Les données collectées sont des données d’identité, de personnalité et des données de santé.
        </p>
        <p class="mb-2">
            Les données sont collectées auprès de la personne concernée dans le cadre du processus de traitement.
        </p>
        <p class="mb-2">
            Le recueil des données est obligatoire pour la mise en place du traitement.
        </p>
        <p class="mb-12">
            Le traitement ne prévoit pas de prise de décision automatisée.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            7. Licéité des traitements mis en oeuvre
        </h2>
        <p class="mb-12">
            Le traitement repose sur le consentement libre, spécifique, éclairé et univoque de la personne concernée lors de l’initialisation du processus de traitement.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            8. Destinataires des données personnelles
        </h2>
        <p class="mb-2">
            Sont destinataires des données :
        </p>
        <ul class="pl-4">
            <li> - les services et prestataires de RG-EIRL en charge des différentes finalités.</li>
        </ul>
        <p class="mb-2">
            Sont également destinataires des données préalablement anonymisées :
        </p>
        <ul class="pl-4 mb-12">
            <li> - des laboratoires de recherche académique publique et des associations visant à aider les personnes atteintes de troubles auditifs (acouphènes, hyperacousie, malentendance…) ayant fait la demande de ces données et uniquement à des fins de recherche</li>
            <li> - le chercheur indépendant Robin Guillard dans le cadre son activité de recherche sur les acouphènes Robin Guillard EIRL.</li>
            <li> - le projet de recherche européen “Tinnitus Database” uniquement à des fins de recherche</li>
        </ul>

        <h2 class="mb-5 text-xl font-medium">
            9. Transferts des données hors UE
        </h2>
        <p class="mb-12">
            Aucun transfert de données hors de l’Union européenne n’est réalisé.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            10. Durée de conservation
        </h2>
        <p class="mb-12">
            Les données des personnes concernées sont conservées en base active pendant 4 ans puis anonymisées.
        </p>

        <h2 class="mb-5 text-xl font-medium">
            11. Droits de l'Utilisateur sur les données le/la concernant
        </h2>
        <p class="mb-2">
            L'Utilisateur bénéficie sur ses données personnelles d’un droit d’accès, de rectification, et d’effacement, d’un droit à la portabilité, à la limitation du traitement, et d’opposition dans les conditions prévues par la réglementation.
        </p>
        <p class="mb-12">
            Le délégué à la protection des données (DPO) de RG-EIRL est votre interlocuteur pour toute demande d’exercice de vos droits sur ce traitement. Contacter le DPO par voie électronique : <a href="mailto:dpo_siopi@rgpdbox.com" class="text-sky-500">dpo_siopi@rgpdbox.com</a>
        </p>

        <h2 class="mb-5 text-xl font-medium">
            12. Réclamation (plainte) auprès de la CNIL
        </h2>
        <p class="mb-12">
            Si vous estimez, après nous avoir contactés, que vos droits sur vos données ne sont pas respectés, vous pouvez adresser une réclamation (plainte) auprès de la CNIL, 3 place de Fontenoy – TSA 80715 – 75334 PARIS CEDEX 07.
        </p>
    </div>
@endsection
