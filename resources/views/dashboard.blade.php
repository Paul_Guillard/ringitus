@extends('layouts.app')

@section('content')
    <div style="display: none" class="relative z-10 flex flex-wrap justify-between w-full min-h-[100vh] pt-20" x-data="{ section: 'general' }" x-show="section">
        <div class="w-full px-5 pt-12 lg:w-1/4">
            <nav class="sticky block p-5 shadow-xl top-32 bg-gradient-to-r from-sky-400 to-green-300 rounded-xl">
                {{-- <div class="absolute top-0 right-0 w-full h-full bg-gradient-to-r from-transparent to-white"></div> --}}
                <ul class="text-lg lg:text-xl">
                    <li class="mb-4">
                        <button class="font-light text-white transition-all hover:pl-3" :class="section === 'general' && 'pl-3'" @click="section = 'general'">
                            <i class="mr-2 fa-solid fa-eye"></i> Vue d'ensemble
                        </button>
                    </li>
                    @if (auth()->user()->role === \App\Enums\UserRoles::Admin)
                    <li class="mb-4">
                        <a href="/admin" class="font-light text-white transition-all hover:pl-3">
                            <i class="mr-2 fa-solid fa-user-tie"></i> Espace administrateur
                        </a>
                    </li>
                    @endif
                    <li class="mb-4">
                        <button class="font-light text-white transition-all hover:pl-3" :class="section === 'rings' && 'pl-3'" @click="section = 'rings'">
                            <i class="mr-2 fa-solid fa-mobile"></i> Mes sonneries
                        </button>
                    </li>
                    <li class="mb-4">
                        <button class="font-light text-white transition-all hover:pl-3" :class="section === 'profile' && 'pl-3'" @click="section = 'profile'">
                            <i class="mr-2 fa-solid fa-user"></i> Données personnelles
                        </button>
                    </li>
                    <li>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button type="submit" class="font-light text-white transition-all hover:pl-2">
                                <i class="mr-2 fa-solid fa-right-from-bracket"></i> Se déconnecter
                            </button>
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
        <section class="w-full lg:w-3/4 lg:pl-10">
            <h1 class="mb-5 text-xl font-medium text-center lg:text-left lg:text-3xl text-sky-500 py-7">
                Bienvenue sur votre espace personnel, <span class="text-green-400">{{ ucfirst(auth()->user()->first_name) }}</span>
            </h1>
            <div>
                <div x-show="section === 'general'">
                    <div class="flex flex-wrap justify-center lg:justify-start mb-7">
                        <div class="w-48 h-48 text-center transition duration-300 shadow-lg hover:shadow-xl rounded-xl bg-sky-500 p-7 m-7 hover:bg-green-400">
                            <h4 class="mb-4 text-xl text-white">Inscrit.e depuis le</h4>
                            <p class="text-2xl font-semibold text-white">
                                {{ Carbon\Carbon::parse(auth()->user()->created_at)->format('d/m/Y') }}
                            </p>
                        </div>
                        <div class="w-48 h-48 text-center transition duration-300 shadow-lg cursor-pointer hover:shadow-xl rounded-xl bg-sky-500 p-7 m-7 hover:bg-green-400" @click="section = 'rings'">
                            <h4 class="mb-4 text-xl text-white">Sonneries créées</h4>
                            <p class="text-4xl font-semibold text-white">
                                {{ auth()->user()->rings()->count() }}
                            </p>
                        </div>
                        <div class="w-48 h-48 text-center transition duration-300 shadow-lg cursor-pointer hover:shadow-xl rounded-xl bg-sky-500 p-7 m-7 hover:bg-green-400" @click="section = 'profile'">
                            <h4 class="mb-4 text-xl text-white">E-mails acceptés</h4>
                            <p class="text-3xl font-semibold text-white">
                                @if (auth()->user()->follow_up_agreed) Oui @else Non @endif
                            </p>
                        </div>
                    </div>
                    <div class="border m-7 rounded-xl p-7 border-slate-200">
                        @foreach ($questions as $question)
                            <h3 class="mb-2 text-xl font-medium text-slate-700">
                                {{ $question->question }}
                            </h3>
                            <p class="mb-5">
                                @if ($question->type === App\Enums\QuestionTypes::MultipleChoice)
                                    @foreach (json_decode($question->pivot->answer, true) as $key => $answer)
                                        @if ($answer) <span class="px-2 py-1 mx-1 text-white rounded bg-sky-500">{{ collect($question->options)->where('key', $key)->first()['answer'] }}</span> @endif
                                    @endforeach
                                @elseif ($question->type === App\Enums\QuestionTypes::Radio)
                                    @if ($question->pivot->answer) <span class="px-2 py-1 mx-1 text-white rounded bg-sky-500">{{ $question->pivot->answer }}</span> @endif
                                @else
                                    <span class="px-2 py-1 mx-1 text-white rounded bg-sky-500">{{ $question->pivot->answer }} / 10</span>
                                @endif
                            </p>
                        @endforeach
                    </div>
                </div>
                <div x-show="section === 'rings'">
                    @livewire('dashboard.rings')
                </div>
                <div x-show="section === 'profile'">
                    @livewire('profile')
                </div>
            </div>
        </section>
    </div>
@endsection
