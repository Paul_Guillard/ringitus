<?php

namespace Tests\Feature;

use App\Enums\SoundCategories;
use App\Enums\UserRoles;
use App\Livewire\Dashboard\Rings;
use App\Livewire\Layout\Contact;
use App\Livewire\Sounds\SoundMixer;
use App\Models\Sound;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PagesDisplayTest extends TestCase
{
    use RefreshDatabase;

    private User $user;
    private User $admin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->admin = User::factory([
            'role' => UserRoles::Admin,
        ])->create();
    }

    /**
     * @test
     */
    public function home_page_displays(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function nav_displays_login_and_register_link_for_guests(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('Se connecter');
        $response->assertSee('Créer un profil');
    }

    /**
     * @test
     */
    public function nav_displays_dashboard_link_for_users(): void
    {
        $response = $this->actingAs($this->user)->get('/');

        $response->assertStatus(200);
        $response->assertSee('Mon tableau de bord');
    }

    /**
     * @test
     */
    public function ringitus_page_displays(): void
    {
        $response = $this->get('/reveil-matin-acouphenes');

        $response->assertStatus(200);
        $response->assertSeeLivewire(SoundMixer::class);
    }

    /**
     * @test
     */
    public function sounds_display_on_ringitus_page(): void
    {
        $sounds = Sound::factory(10)->create();

        $response = $this->get('/reveil-matin-acouphenes');

        $response->assertSee('Nos suggestions');
        $response->assertSee('Sons de la nature');
        $response->assertSee('Bruits blancs');
        // Test below fails because of lazy loading
        // foreach ($sounds->where('category', '<>', SoundCategories::Wakeup)->all() as $sound) {
        //     $response->assertSee(ucfirst($sound->name));
        // }
    }

    /**
     * @test
     */
    public function ring_creation_is_not_possible_for_guests(): void
    {
        $response = $this->get('/reveil-matin-acouphenes');

        $response->assertDontSee('Ma sélection');
    }

    /**
     * @test
     */
    public function ring_creation_is_possible_for_users(): void
    {
        $sounds = Sound::factory(10)->create();
        $wakeups = Sound::factory(3)->create([
            'category' => SoundCategories::Wakeup,
        ]);

        $response = $this->actingAs($this->user)->get('/reveil-matin-acouphenes');

        $response->assertDontSee("Créez un compte ou connectez-vous pour créer votre sonnerie");
        // Test below do not work because of lazy loading
        // foreach ($sounds->where('category', '<>', SoundCategories::Wakeup)->all() as $sound) {
        //     $response->assertSee(ucfirst($sound->name));
        // }

        // foreach ($wakeups as $sound) {
        //     $response->assertSee($sound->name);
        // }

        // $response->assertSee('Ma sélection');
    }

    /**
     * @test
     */
    public function contact_page_displays(): void
    {
        $response = $this->get('/nous-contacter');

        $response->assertStatus(200);
        $response->assertSeeLivewire(Contact::class);
    }

    /**
     * @test
     */
    public function general_conditions_page_displays(): void
    {
        $response = $this->get('/conditions-generales-d-utilisation');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function legal_page_displays(): void
    {
        $response = $this->get('/mentions-legales');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function privacy_policy_page_displays(): void
    {
        $response = $this->get('/politique-de-confidentialite');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function login_page_displays(): void
    {
        $response = $this->get('/connexion');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function register_page_displays(): void
    {
        $response = $this->get('/inscription');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function reset_password_page_displays(): void
    {
        $response = $this->get('/mot-de-passe/reset');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function dashboard_page_displays_for_users(): void
    {
        $response = $this->actingAs($this->user)->get('/tableau-de-bord');

        $response->assertStatus(200);
        $response->assertSeeLivewire(Rings::class);
    }

    /**
     * @test
     */
    public function dashboard_page_displays_admin_link_for_admin_user(): void
    {
        $response = $this->actingAs($this->admin)->get('/tableau-de-bord');

        $response->assertStatus(200);
        $response->assertSee('Espace administrateur');
    }

    /**
     * @test
     */
    public function dashboard_page_redirects_for_guests(): void
    {
        $response = $this->get('/tableau-de-bord');

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function admin_panel_cannot_be_accessed_by_guests(): void
    {
        $response = $this->get('/admin');

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function admin_panel_cannot_be_accessed_by_users(): void
    {
        $response = $this->actingAs($this->user)->get('/admin');

        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function admin_panel_can_be_accessed_by_admin(): void
    {
        $response = $this->actingAs($this->admin)->get('/admin');

        $response->assertStatus(200);
    }
}
