<?php

namespace Tests\Feature;

use App\Models\User;
use App\Services\BrevoApi;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Volt\Volt;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function profile_page_is_displayed(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/tableau-de-bord');

        $response
            ->assertSeeVolt('profile.update-profile-information-form')
            ->assertSeeVolt('profile.update-password-form')
            ->assertSeeVolt('profile.delete-user-form')
            ->assertOk();
    }

    /**
     * @test
     */
    public function data_usage_agreement_can_be_updated(): void
    {
        $user = User::factory()->create(
            [
                'data_publication_agreed' => false,
            ]
        );

        $response = Volt::actingAs($user)->test('profile')->call('toggleDataPublicationAgreement');

        $this->assertEquals($user->data_publication_agreed, true);

        $response->call('toggleDataPublicationAgreement');

        $this->assertEquals($user->data_publication_agreed, false);
    }

    /**
     * @test
     */
    public function mailing_list_agreement_can_be_updated(): void
    {
        $user = User::factory()->create(
            [
                'follow_up_agreed' => false,
            ]
        );

        // $this->mock(BrevoApi::class)
        //         ->shouldReceive('createContact')
        //         ->with($user->email, $user->first_name, $user->last_name)
        //         ->once();

        // $this->mock(BrevoApi::class)
        //         ->shouldReceive('deleteContact')
        //         ->with($user->email)
        //         ->once();

        $response = Volt::actingAs($user)->test('profile')->call('toggleNewsletterRegistration');

        $this->assertEquals($user->follow_up_agreed, true);

        $response->call('toggleNewsletterRegistration');

        $this->assertEquals($user->follow_up_agreed, false);
    }

    /**
     * @test
     */
    public function profile_information_can_be_updated(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $component = Volt::test('profile.update-profile-information-form')
            ->set('first_name', 'Test')
            ->set('last_name', 'User')
            ->set('email', 'test@example.com')
            ->set('phone', '+33 12 34 56 78')
            ->set('date_of_birth', '01/01/1975')
            ->call('updateProfileInformation');

        $component
            ->assertHasNoErrors()
            ->assertNoRedirect();

        $user->refresh();

        $this->assertSame('Test', $user->first_name);
        $this->assertSame('User', $user->last_name);
        $this->assertSame('test@example.com', $user->email);
        $this->assertNull($user->email_verified_at);
    }

    /**
     * @test
     */
    public function email_verification_status_is_unchanged_when_the_email_address_is_unchanged(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $component = Volt::test('profile.update-profile-information-form')
            ->set('first_name', 'Test')
            ->set('last_name', 'User')
            ->set('email', $user->email)
            ->set('phone', '+33 12 34 56 78')
            ->set('date_of_birth', '01/01/1975')
            ->call('updateProfileInformation');

        $component
            ->assertHasNoErrors()
            ->assertNoRedirect();

        $this->assertNotNull($user->refresh()->email_verified_at);
    }

    /**
     * @test
     */
    public function user_can_delete_their_account(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $component = Volt::test('profile.delete-user-form')
            ->set('password', 'password')
            ->call('deleteUser');

        $component
            ->assertHasNoErrors()
            ->assertRedirect('/');

        $this->assertGuest();
        $this->assertNull($user->fresh());
    }

    /**
     * @test
     */
    public function correct_password_must_be_provided_to_delete_account(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $component = Volt::test('profile.delete-user-form')
            ->set('password', 'wrong-password')
            ->call('deleteUser');

        $component
            ->assertHasErrors('password')
            ->assertNoRedirect();

        $this->assertNotNull($user->fresh());
    }
}
