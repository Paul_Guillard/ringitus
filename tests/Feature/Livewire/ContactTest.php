<?php

namespace Tests\Feature\Livewire;

use App\Livewire\Layout\Contact;
use App\Models\Message;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function contact_page_has_empty_fields_for_guests(): void
    {
        $response = Livewire::test(Contact::class);

        $response->assertViewHas('firstName', '');
        $response->assertViewHas('lastName', '');
        $response->assertViewHas('email', '');
        $response->assertViewHas('message', '');
        $response->assertViewHas('sent', false);
    }

    /**
     * @test
     */
    public function contact_page_has_prefilled_fields_for_users(): void
    {
        $user = User::factory()->create();

        $response = Livewire::actingAs($user)->test(Contact::class);

        $response->assertViewHas('firstName', $user->first_name);
        $response->assertViewHas('lastName', $user->last_name);
        $response->assertViewHas('email', $user->email);
        $response->assertViewHas('message', '');
        $response->assertViewHas('sent', false);
    }

    /**
     * @test
     */
    public function normal_contact_message_gets_sent_successfully(): void
    {
        config()->set('honeypot.enabled', false);
        $response = Livewire::test(Contact::class)
                            ->set('firstName', 'John')
                            ->set('lastName', 'Doe')
                            ->set('email', 'john.doe@mail.com')
                            ->set('message', 'Some random text message for testing')
                            ->call('save');

        $response->assertSet('sent', true);
        $this->assertEquals(Message::count(), 1);
        $message = Message::first();
        $this->assertEquals($message->first_name, 'John');
        $this->assertEquals($message->last_name, 'Doe');
        $this->assertEquals($message->email, 'john.doe@mail.com');
        $this->assertEquals($message->message, 'Some random text message for testing');
        $this->assertEquals($message->is_read, false);
    }

    /**
     * @test
     */
    public function contact_message_fails_when_all_data_is_missing(): void
    {
        config()->set('honeypot.enabled', false);

        $this->assertEquals(Message::count(), 0);

        $response = Livewire::test(Contact::class)->set('message', '')->call('save');

        $response->assertSet('sent', false);
        $response->assertHasErrors('firstName');
        $response->assertHasErrors('lastName');
        $response->assertHasErrors('email');
        $response->assertHasErrors('message');

        $this->assertEquals(Message::count(), 0);
    }

    /**
     * @test
     */
    public function contact_message_fails_when_message_is_missing(): void
    {
        $user = User::factory()->create();
        config()->set('honeypot.enabled', false);

        $this->assertEquals(Message::count(), 0);

        $response = Livewire::actingAs($user)->test(Contact::class)->set('message', '')->call('save');

        $response->assertSet('sent', false);
        $response->assertHasNoErrors('firstName');
        $response->assertHasNoErrors('lastName');
        $response->assertHasNoErrors('email');
        $response->assertHasErrors('message');

        $this->assertEquals(Message::count(), 0);
    }
}
