<?php

namespace Tests\Feature\Livewire;

use App\Livewire\Dashboard\Rings;
use App\Models\Ring;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Tests\TestCase;

class RingsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function link_is_present_when_no_rings_are_in_the_list(): void
    {
        $user = User::factory()->create();

        $response = Livewire::actingAs($user)->test(Rings::class);

        $response->assertSee('Aucune sonnerie créée pour le moment...');
        $response->assertSee('Créer une sonnerie');
    }

    /**
     * @test
     */
    public function user_rings_are_listed(): void
    {
        $user = User::factory()->create();
        $rings = Ring::factory(10)->create([
            'user_id' => $user->id,
        ]);

        $response = Livewire::actingAs($user)->test(Rings::class);

        $response->assertSee('Télécharger');
        $response->assertSee('Supprimer');
        $response->assertViewHas('rings', $rings);
    }

    /**
     * @test
     */
    public function user_rings_can_be_downloaded_and_deleted(): void
    {
        $user = User::factory()->create();
        $ring = Ring::factory()->create([
            'file' => 'test/test-file.mp3',
            'user_id' => $user->id,
        ]);

        $response = Livewire::actingAs($user)->test(Rings::class);

        // Create fake file in test folder
        Storage::putFileAs('public', UploadedFile::fake()->create($ring->file, 2000), $ring->file, 'public');
        Storage::assertExists('public/' . $ring->file);

        // Simulate ring download and assert
        $response->call('downloadRing', $ring->id);
        $response->assertFileDownloaded();

        // Simulate ring delete and assert
        $response->call('deleteRing', $ring->id);
        Storage::assertMissing('public/' . $ring->file);

        // Delete test directory if still present
        Storage::deleteDirectory('public/test');
    }
}
