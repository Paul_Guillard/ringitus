<?php

namespace Tests\Feature\Auth;

use App\Enums\QuestionTypes;
use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Livewire\Livewire;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function registration_page_contains_livewire_component()
    {
        $this->get(route('register'))
            ->assertSuccessful()
            ->assertSeeLivewire('auth.register');
    }

    /** @test */
    public function is_redirected_if_already_logged_in()
    {
        $user = User::factory()->create();

        $this->be($user);

        $this->get(route('register'))
            ->assertRedirect(route('home'));
    }

    /** @test */
    public function a_user_can_register()
    {
        $question1 = Question::factory()->create();
        $question2 = Question::factory()->create();
        $question3 = Question::factory()->create([
            'type' => QuestionTypes::Range,
            'options' => null,
        ]);

        Event::fake();

        $response = Livewire::test('auth.register')
            ->set('firstName', 'Tall')
            ->set('lastName', 'Stack')
            ->set('email', 'tallstack@example.com')
            ->set('phone', '+33 12 34 56 78')
            ->set('dateOfBirth', '01/01/1975')
            ->set('password', 'password')
            ->set('passwordConfirmation', 'password')
            ->set('answers', [
                $question1->id => [
                    'abcde-0' => true,
                    'abcde-1' => true,
                    'abcde-2' => false,
                ],
                $question2->id => [
                    'abcde-0' => false,
                    'abcde-1' => true,
                    'abcde-2' => false,
                ],
                $question3->id => 5,
            ])
            ->set('conditionsAccepted', true)
            ->set('followupAccepted', false)
            ->set('publishedDataAccepted', true)
            ->call('register');

        $response->assertRedirect(route('dashboard'));
        $this->assertTrue(User::whereEmail('tallstack@example.com')->exists());
        $this->assertEquals('tallstack@example.com', Auth::user()->email);
        $this->assertEquals(1, Auth::user()->data_publication_agreed);
        $response->assertHasNoErrors('multiple');

        Event::assertDispatched(Registered::class);
    }

    /** @test */
    public function test_at_least_one_checkbox_must_be_checked_for_each_question()
    {
        $question1 = Question::factory()->create();
        $question2 = Question::factory()->create();
        $question3 = Question::factory()->create([
            'type' => QuestionTypes::Range,
            'options' => null,
        ]);

        Event::fake();

        $response = Livewire::test('auth.register')
            ->set('firstName', 'Tall')
            ->set('lastName', 'Stack')
            ->set('email', 'tallstack@example.com')
            ->set('phone', '+33 12 34 56 78')
            ->set('dateOfBirth', '01/01/1975')
            ->set('password', 'password')
            ->set('passwordConfirmation', 'password')
            ->set('answers', [
                $question1->id => [
                    'abcde-0' => false,
                    'abcde-1' => false,
                    'abcde-2' => true,
                ],
                $question2->id => [
                    'abcde-0' => false,
                    'abcde-1' => false,
                    'abcde-2' => false,
                ],
                $question3->id => 5,
            ])
            ->set('conditionsAccepted', true)
            ->set('followupAccepted', false)
            ->set('publishedDataAccepted', true)
            ->call('register');

        $response->assertHasErrors('multiple');
    }

    /** @test */
    public function test_conditions_must_be_agreed_to_register()
    {
        $question1 = Question::factory()->create();
        $question2 = Question::factory()->create();

        Event::fake();

        $response = Livewire::test('auth.register')
            ->set('firstName', 'Tall')
            ->set('lastName', 'Stack')
            ->set('email', 'tallstack@example.com')
            ->set('password', 'password')
            ->set('passwordConfirmation', 'password')
            ->set('answers', [
                $question1->id => [
                    'abcde-0' => false,
                    'abcde-1' => false,
                    'abcde-2' => true,
                ],
                $question2->id => [
                    'abcde-0' => false,
                    'abcde-1' => false,
                    'abcde-2' => false,
                ],
            ])
            ->set('conditionsAccepted', false)
            ->set('followupAccepted', false)
            ->set('publishedDataAccepted', false)
            ->call('register');

        $response->assertHasErrors(['conditionsAccepted' => 'accepted']);
        $response->assertHasNoErrors('followupAccepted');
        $response->assertHasNoErrors('publishedDataAccepted');
    }

    /** @test */
    public function first_name_is_required()
    {
        Livewire::test('auth.register')
            ->set('firstName', '')
            ->call('register')
            ->assertHasErrors(['firstName' => 'required']);
    }

    /** @test */
    public function last_name_is_required()
    {
        Livewire::test('auth.register')
            ->set('lastName', '')
            ->call('register')
            ->assertHasErrors(['lastName' => 'required']);
    }

    /** @test */
    public function email_is_required()
    {
        Livewire::test('auth.register')
            ->set('email', '')
            ->call('register')
            ->assertHasErrors(['email' => 'required']);
    }

    /** @test */
    public function email_is_valid_email()
    {
        Livewire::test('auth.register')
            ->set('email', 'tallstack')
            ->call('register')
            ->assertHasErrors(['email' => 'email']);
    }

    /** @test */
    public function email_hasnt_been_taken_already()
    {
        User::factory()->create(['email' => 'tallstack@example.com']);

        Livewire::test('auth.register')
            ->set('email', 'tallstack@example.com')
            ->call('register')
            ->assertHasErrors(['email' => 'unique']);
    }

    /** @test */
    public function see_email_hasnt_already_been_taken_validation_message_as_user_types()
    {
        User::factory()->create(['email' => 'tallstack@example.com']);

        Livewire::test('auth.register')
            ->set('email', 'smallstack@gmail.com')
            ->assertHasNoErrors()
            ->set('email', 'tallstack@example.com')
            ->call('register')
            ->assertHasErrors(['email' => 'unique']);
    }

    /** @test */
    public function password_is_required()
    {
        Livewire::test('auth.register')
            ->set('password', '')
            ->set('passwordConfirmation', 'password')
            ->call('register')
            ->assertHasErrors(['password' => 'required']);
    }

    /** @test */
    public function password_is_minimum_of_eight_characters()
    {
        Livewire::test('auth.register')
            ->set('password', 'secret')
            ->set('passwordConfirmation', 'secret')
            ->call('register')
            ->assertHasErrors(['password' => 'min']);
    }

    /** @test */
    public function password_matches_password_confirmation()
    {
        Livewire::test('auth.register')
            ->set('email', 'tallstack@example.com')
            ->set('password', 'password')
            ->set('passwordConfirmation', 'not-password')
            ->call('register')
            ->assertHasErrors(['password' => 'same']);
    }
}
