<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

class PasswordConfirmationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function confirm_password_screen_can_be_rendered(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('password.confirm'));

        $response
            ->assertSeeLivewire('auth.passwords.confirm')
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function password_can_be_confirmed(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $component = Livewire::test('auth.passwords.confirm')
            ->set('password', 'password');

        $component->call('confirm');

        $component
            ->assertRedirect(route('dashboard'))
            ->assertHasNoErrors();
    }

    /**
     * @test
     */
    public function password_is_not_confirmed_with_invalid_password(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $component = Livewire::test('auth.passwords.confirm')
            ->set('password', 'wrong-password');

        $component->call('confirm');

        $component
            ->assertNoRedirect()
            ->assertHasErrors('password');
    }
}
