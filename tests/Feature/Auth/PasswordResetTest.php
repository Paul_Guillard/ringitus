<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Livewire\Livewire;
use Tests\TestCase;

class PasswordResetTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function reset_password_link_screen_can_be_rendered(): void
    {
        $response = $this->get('/mot-de-passe/reset');

        $response
            ->assertSeeLivewire('auth.passwords.email')
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function reset_password_link_can_be_requested(): void
    {
        Notification::fake();

        $user = User::factory()->create();

        Livewire::test('auth.passwords.email')
            ->set('email', $user->email)
            ->call('sendResetPasswordLink');

        Notification::assertSentTo($user, ResetPassword::class);
    }

    /**
     * @test
     */
    public function reset_password_screen_can_be_rendered(): void
    {
        Notification::fake();

        $user = User::factory()->create();

        Livewire::test('auth.passwords.email')
            ->set('email', $user->email)
            ->call('sendResetPasswordLink');

        Notification::assertSentTo($user, ResetPassword::class, function ($notification) {
            $response = $this->get('/mot-de-passe/reset/' . $notification->token);

            $response
                ->assertSeeLivewire('auth.passwords.reset')
                ->assertStatus(200);

            return true;
        });
    }

    /**
     * @test
     */
    public function password_can_be_reset_with_valid_token(): void
    {
        Notification::fake();

        $user = User::factory()->create();

        Livewire::test('auth.passwords.email')
            ->set('email', $user->email)
            ->call('sendResetPasswordLink');

        Notification::assertSentTo($user, ResetPassword::class, function ($notification) use ($user) {
            $component = Livewire::test('auth.passwords.reset   ', ['token' => $notification->token])
                ->set('email', $user->email)
                ->set('password', 'password')
                ->set('passwordConfirmation', 'password');

            $component->call('resetPassword');

            $component
                ->assertRedirect(route('dashboard'))
                ->assertHasNoErrors();

            return true;
        });
    }
}
