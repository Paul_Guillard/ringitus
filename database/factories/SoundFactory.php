<?php

namespace Database\Factories;

use App\Enums\SoundCategories;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Sound>
 */
class SoundFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'raw_file' => 'raw_sounds/' . Str::random(26) . '.mp3',
            'file' => 'sounds/' . Str::random(6) . '-' . fake()->slug(3) . '.mp3',
            'sample' => 'samples/' . Str::random(6) . '-' . fake()->slug(3) . '-sample-1min.mp3',
            'name' => fake()->words(3, true),
            'category' => SoundCategories::getRandomValue(),
            'uses' => 0,
            'is_active' => true,
        ];
    }
}
