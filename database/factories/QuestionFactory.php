<?php

namespace Database\Factories;

use App\Enums\QuestionTypes;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Question>
 */
class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'question' => fake()->words(7, true) . '?',
            'type' => QuestionTypes::MultipleChoice,
            'options' => [
                [
                    'key' => 'abcde-0',
                    'answer' => fake()->words(3, true),
                ],
                [
                    'key' => 'abcde-1',
                    'answer' => fake()->words(3, true),
                ],
                [
                    'key' => 'abcde-2',
                    'answer' => fake()->words(3, true),
                ],
            ],
            'visible' => true,
        ];
    }
}
