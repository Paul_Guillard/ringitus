<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        if (app()->environment('local')) {
            \App\Models\User::factory()->create([
                'first_name' => 'User',
                'last_name' => 'Test',
                'email' => 'test@mail.com',
            ]);
        }
    }
}
