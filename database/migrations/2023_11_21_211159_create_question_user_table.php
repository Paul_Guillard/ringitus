<?php

use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('question_user', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(Question::class)->constrained();
            $table->foreignIdFor(User::class)->constrained();
            $table->string('answer')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('question_user');
    }
};
