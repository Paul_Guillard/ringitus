<?php

use App\Enums\SoundCategories;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('sounds', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('raw_file', 125);
            $table->string('file', 150)->nullable();
            $table->string('sample', 150)->nullable();
            $table->string('name', 150);
            $table->string('category')->default(SoundCategories::Tinnitus);
            $table->integer('uses')->default(0);
            $table->boolean('is_active')->default(false);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('sounds');
    }
};
