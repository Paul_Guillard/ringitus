<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->boolean('is_read')->default(false);
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email', 150);
            $table->text('message');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('messages');
    }
};
