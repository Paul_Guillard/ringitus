<?php

use App\Enums\QuestionTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('question');
            $table->string('type')->default(QuestionTypes::MultipleChoice);
            $table->json('options')->nullable();
            $table->boolean('visible')->default(false);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('questions');
    }
};
