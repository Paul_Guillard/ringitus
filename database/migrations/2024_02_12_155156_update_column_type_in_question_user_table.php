<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('question_user', function (Blueprint $table) {
            $table->text('answer')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table('question_user', function (Blueprint $table) {
            $table->string('answer')->nullable()->change();
        });
    }
};
