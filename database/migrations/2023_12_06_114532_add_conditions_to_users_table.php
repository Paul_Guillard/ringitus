<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('conditions_agreed')->default(false);
            $table->boolean('follow_up_agreed')->default(false);
            $table->boolean('data_publication_agreed')->default(false);
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('conditions_agreed');
            $table->dropColumn('follow_up_agreed');
            $table->dropColumn('data_publication_agreed');
        });
    }
};
