<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Votre mot de passe a été mis à jour.',
    'sent' => 'Nous vous avons envoyé un lien pour mettre à jour votre mot de passe par email.',
    'throttled' => 'Merci de patienter avant de réessayer.',
    'token' => 'Le token de mise à jour du mot de passe est invalide.',
    'user' => "Nous ne parvenons pas à trouver un utilisateur avec cette adresse email.",

];
