<?php

namespace App\Livewire\Dashboard;

use App\Models\Ring;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class Rings extends Component
{
    public array $names = [];

    public function mount(): void
    {
        foreach (auth()->user()->rings as $ring) {
            $this->names[$ring->id] = ucfirst($ring->name);
        }
    }

    public function updatedNames($value, $key): void
    {
        if ($ring = Ring::find($key)) {
            $ring->name = strtolower($value);
            $ring->save();
        }
    }

    public function deleteRing(int $id): void
    {
        if ($ring = Ring::find($id)) {
            Storage::disk('public')->delete($ring->file);
            Storage::disk('public')->deleteDirectory(explode('/', $ring->file)[0]);
            $ring->delete();
        }
    }

    public function downloadRing(int $id)
    {
        if ($ring = Ring::find($id)) {
            return Storage::disk('public')->download($ring->file);
        }
    }

    public function render()
    {
        return view('livewire.dashboard.rings', [
            'rings' => auth()->user()->rings,
        ]);
    }
}
