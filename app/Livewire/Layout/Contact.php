<?php

namespace App\Livewire\Layout;

use App\Models\Message;
use Livewire\Component;
use Spatie\Honeypot\Http\Livewire\Concerns\HoneypotData;
use Spatie\Honeypot\Http\Livewire\Concerns\UsesSpamProtection;

class Contact extends Component
{
    use UsesSpamProtection;

    public string $firstName = '';
    public string $lastName = '';
    public string $email = '';
    public string $message = '';
    public bool $sent = false;
    public HoneypotData $extraFields;

    protected $rules = [
        'firstName' => 'string|min:2|max:50|required',
        'lastName' => 'string|min:1|max:50|required',
        'email' => 'email|required',
        'message' => 'string|min:1|max:2000|required',
    ];

    public function mount(): void
    {
        $this->extraFields = new HoneypotData;
        if (auth()->check()) {
            $this->firstName = auth()->user()->first_name;
            $this->lastName = auth()->user()->last_name;
            $this->email = auth()->user()->email;
        }
    }

    public function save(): void
    {
        $this->protectAgainstSpam();
        $this->validate();

        $message = new Message;
        $message->first_name = $this->firstName;
        $message->last_name = $this->lastName;
        $message->email = $this->email;
        $message->message = $this->message;
        $message->save();
        $this->reset();
        $this->sent = true;
    }

    public function render()
    {
        return view('livewire.layout.contact');
    }
}
