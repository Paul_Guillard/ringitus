<?php

namespace App\Livewire\Auth;

use App\Enums\QuestionTypes;
use App\Models\Question;
use App\Models\User;
use App\Services\BrevoApi;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Livewire\Component;

class Register extends Component
{
    /** @var string */
    public string $firstName = '';

    /** @var string */
    public string $lastName = '';

    /** @var string */
    public ?string $dateOfBirth = null;

    /** @var string */
    public string $email = '';

    /** @var string */
    public string $phone = '';

    /** @var string */
    public string $password = '';

    /** @var string */
    public string $passwordConfirmation = '';

    public int $step = 0;

    public ?Collection $questions = null;

    public array $answers = [];

    public bool $conditionsAccepted = false;
    public bool $followupAccepted = false;
    public bool $publishedDataAccepted = false;

    public function mount(): void
    {
        $this->questions = Question::where('visible', '1')->orderBy('position')->get();
        foreach ($this->questions as $question) {
            if (in_array($question->type, [QuestionTypes::MultipleChoice, QuestionTypes::Radio])) {
                foreach ($question->options as $option) {
                    $this->answers[$question->id][$option['key']] = null;
                }
            } else {
                $this->answers[$question->id] = 5;
            }
        }
    }

    public function checkPersonalInfos(): void
    {
        $this->step = 0;

        $this->validate([
            'firstName' => ['required', 'string', 'min:1', 'max:50'],
            'lastName' => ['required', 'string', 'min:2', 'max:50'],
            'dateOfBirth' => ['required', 'date'],
            'email' => ['required', 'email', 'unique:users'],
            'phone' => ['required', 'string', 'min:6', 'max:20'],
            'password' => ['required', 'min:8', 'same:passwordConfirmation'],
        ]);

        $this->step = 1;
    }

    public function register()
    {
        $this->validate([
            'firstName' => ['required', 'string', 'min:1', 'max:50'],
            'lastName' => ['required', 'string', 'min:2', 'max:50'],
            'dateOfBirth' => ['required', 'date'],
            'email' => ['required', 'email', 'unique:users'],
            'phone' => ['required', 'string', 'min:6', 'max:20'],
            'password' => ['required', 'min:8', 'same:passwordConfirmation'],
            'answers' => ['array', Rule::requiredIf($this->questions->count() > 0)],
            'answers.*' => ['required'],
            'conditionsAccepted' => ['accepted'],
            'followupAccepted' => ['nullable'],
            'publishedDataAccepted' => ['nullable'],
        ]);

        foreach ($this->answers as $answer) {
            if (is_array($answer)) {
                if (array_search(true, $answer) === false) {
                    $this->addError('multiple', 'Au moins une réponse est requise aux questions à choix multiple.');
                    return;
                }
            }
        }

        $user = User::create([
            'email' => $this->email,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'date_of_birth' => $this->dateOfBirth,
            'phone' => preg_replace('/[^0-9+]/', '', $this->phone),
            'password' => Hash::make($this->password),
            'conditions_agreed' => true,
            'follow_up_agreed' => $this->followupAccepted,
            'data_publication_agreed' => $this->publishedDataAccepted,
        ]);

        event(new Registered($user));

        Auth::login($user, true);

        foreach ($this->answers as $question => $answer) {
            if (is_array($answer)) {
                $user->questions()->attach($question, ['answer' => json_encode($answer)]);
            } else {
                $user->questions()->attach($question, ['answer' => $answer]);
            }
        }

        if ($this->followupAccepted) {
            try {
                BrevoApi::createContact($this->email, $this->firstName, $this->lastName, $this->dateOfBirth);
            } catch (Exception $e) {
            }
        }

        try {
            BrevoApi::sendRegistrationEmail($user->email, $user->first_name, $user->last_name);
        } catch (Exception $e) {
        }

        return redirect()->route('dashboard');
    }

    public function render()
    {
        return view('livewire.auth.register')->extends('layouts.app');
    }
}
