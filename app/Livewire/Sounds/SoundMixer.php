<?php

namespace App\Livewire\Sounds;

use App\Enums\SoundCategories;
use App\Models\Sound;
use App\Services\SoundManager;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class SoundMixer extends Component
{
    public array $selectedSounds = [];
    public array $selectedVolumes = [];
    public ?int $extraWakeUp = null;
    public string $name = '';
    public ?string $ring = null;
    public bool $generating = false;

    public function placeholder(): View
    {
        return view('includes.placeholders.sound-lists');
    }

    public function addToSelection(int $id): void
    {
        if (!isset($this->selectedSounds[$id]) && Sound::where('is_active', '1')->where('id', $id)->count()) {
            $this->selectedSounds[$id] = Sound::find($id);
            $this->selectedVolumes[$id] = 10;
        }
    }

    public function removeSound(int $id): void
    {
        if (isset($this->selectedSounds[$id]) && Sound::where('is_active', '1')->where('id', $id)->count()) {
            unset($this->selectedSounds[$id]);
            unset($this->selectedVolumes[$id]);
        }
    }

    public function generateRing(): void
    {
        $filesToMerge = [];

        foreach ($this->selectedSounds as $id => $sound) {
            array_push($filesToMerge, [$sound->faded_track, $this->selectedVolumes[$id]]);
        }

        $this->ring = SoundManager::generateRing($filesToMerge, $this->extraWakeUp, $this->name);
    }

    public function downloadRing()
    {
        return Storage::disk('public')->download($this->ring);
    }

    public function chooseWakeUp(int $id): void
    {
        if (Sound::where('is_active', '1')->where('category', SoundCategories::Wakeup)->where('id', $id)->count()) {
            $this->extraWakeUp = $id;
        }
    }

    public function removeWakeUp(): void
    {
        $this->extraWakeUp = null;
    }

    public function resetSounds(): void
    {
        $this->selectedSounds = [];
        $this->selectedVolumes = [];
        $this->extraWakeUp = null;
        $this->name = '';
        $this->ring = null;
        $this->generating = false;
    }

    public function render()
    {
        $natureSounds = Sound::query()
                                ->where('is_active', '1')
                                ->where('category', SoundCategories::Nature)
                                ->orderBy('created_at')
                                ->get();
        $tinnitusSounds = Sound::query()
                                ->where('is_active', '1')
                                ->where('category', SoundCategories::Tinnitus)
                                ->orderBy('created_at')
                                ->get();
        $suggestedSounds = Sound::query()
                                ->where('is_active', '1')
                                ->where('category', SoundCategories::Suggestion)
                                ->orderBy('created_at')
                                ->get();
        $wakeupSounds = Sound::query()
                                ->where('is_active', '1')
                                ->where('category', SoundCategories::Wakeup)
                                ->orderBy('created_at')
                                ->get();
        return view('livewire.sounds.sound-mixer', [
            'natureSounds' => $natureSounds,
            'tinnitusSounds' => $tinnitusSounds,
            'wakeupSounds' => $wakeupSounds,
            'suggestedSounds' => $suggestedSounds,
        ]);
    }
}
