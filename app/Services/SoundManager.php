<?php

namespace App\Services;

use App\Models\Ring;
use App\Models\Sound;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Audio\AudioClipFilter;
use FFMpeg\Filters\Audio\CustomFilter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ProtoneMedia\LaravelFFMpeg\Filesystem\Media;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class SoundManager
{
    const RING_DURATION = 180;
    const WAKEUP_START = 120;
    const FADE_UP_DURATION = 90;

    public static function convertToMp3(Sound $sound): void
    {
        $newFileName = 'sounds/' . Str::random(6) . '-' . Str::slug($sound->name) . '.mp3';
        FFMpeg::fromDisk('public')
                ->open($sound->raw_file)
                ->export()
                ->toDisk('public')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save($newFileName);
        $sound->file = $newFileName;
        $sound->save();
    }

    public static function createSample(Sound $sound, int $duration, ?int $offset = 0): void
    {
        // $duration in seconds
        $newSampleName = 'samples/' . Str::random(6) . '-' . Str::slug($sound->name) . '-sample-' . $duration . 's.mp3';
        $media = FFMpeg::fromDisk('public')->open($sound->file);
        $rawDuration = $media->getDurationInSeconds();

        if ($rawDuration > $duration) {
            $start = TimeCode::fromSeconds($offset);
            $end = TimeCode::fromSeconds($duration);
            $clipFilter = new AudioClipFilter($start, $end);
            $media->addFilter($clipFilter)
                ->export()
                ->toDisk('public')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save($newSampleName);
            $sound->sample = $newSampleName;
            $sound->save();
        } elseif ($rawDuration < $duration) {
            $concatCount = ceil($duration / $rawDuration);
            $tempName = 'samples/' . Str::random(10) . '-temp.mp3';
            $concatSounds = [];
            for ($i = 0; $i < $concatCount ; $i++) {
                array_push($concatSounds, $sound->file);
            }
            FFMpeg::fromDisk('public')
                            ->open($concatSounds)
                            ->export()
                            ->concatWithoutTranscoding()
                            ->save($tempName);
            $rawDuration = FFMpeg::fromDisk('public')->open($tempName)->getDurationInSeconds();

            $start = TimeCode::fromSeconds($offset);
            $end = TimeCode::fromSeconds($duration);
            $clipFilter = new AudioClipFilter($start, $end);
            FFMpeg::fromDisk('public')
                        ->open($tempName)
                        ->addFilter($clipFilter)
                        ->export()
                        ->toDisk('public')
                        ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                        ->save($newSampleName);
            $sound->sample = $newSampleName;
            $sound->save();
            Storage::delete('public/' . $tempName);
        } else {
            $media->export()
                ->toDisk('public')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save($newSampleName);
            $sound->sample = $newSampleName;
            $sound->save();
        }
    }

    // Create a 3min track, with volume faded from 0 to max through the whole sound
    public static function createFadedTrack(Sound $sound, int $duration = self::RING_DURATION): void
    {
        $offset = 0;
        // $duration in seconds
        $newSampleName = 'faded/' . Str::random(6) . '-' . Str::slug($sound->name) . '-faded-' . $duration . 's.mp3';
        $media = FFMpeg::fromDisk('public')->open($sound->file);
        $rawDuration = $media->getDurationInSeconds();

        // Create the clip filter
        $start = TimeCode::fromSeconds($offset);
        $end = TimeCode::fromSeconds($duration);
        $clipFilter = new AudioClipFilter($start, $end);

        // Create the fade filter
        $fadeDuration = self::FADE_UP_DURATION; //(int) floor($duration / 2);
        $fadeFilter = new CustomFilter('afade=t=in:ss=0:d=' . $fadeDuration);

        // Create a sound file of the right duration, by clipping or concatenating
        // Fading filter is directly applied to the sound
        if ($rawDuration > $duration) {
            $media->addFilter($clipFilter)
                ->addFilter($fadeFilter)
                ->export()
                ->toDisk('public')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save($newSampleName);
        } elseif ($rawDuration < $duration) {
            $concatCount = ceil($duration / $rawDuration);
            $tempName = 'samples/' . Str::random(10) . '-temp.mp3';
            $concatSounds = [];
            for ($i = 0; $i < $concatCount ; $i++) {
                array_push($concatSounds, $sound->file);
            }
            FFMpeg::fromDisk('public')
                            ->open($concatSounds)
                            ->export()
                            ->concatWithoutTranscoding()
                            ->save($tempName);
            $rawDuration = FFMpeg::fromDisk('public')->open($tempName)->getDurationInSeconds();

            FFMpeg::fromDisk('public')
                        ->open($tempName)
                        ->addFilter($clipFilter)
                        ->addFilter($fadeFilter)
                        ->export()
                        ->toDisk('public')
                        ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                        ->save($newSampleName);
            Storage::delete('public/' . $tempName);
        }

        $sound->faded_track = $newSampleName;
        $sound->save();
    }

    // Create a 3min track, with silence for 2min before full sound
    public static function createFullWakeUp(Sound $sound): void
    {
        $duration = self::RING_DURATION;
        $silenceDuration = self::WAKEUP_START;
        $offset = 0;
        // $duration in seconds
        $newSampleName = 'faded/' . Str::random(6) . '-' . Str::slug($sound->name) . '-silenced-' . $duration . 's.mp3';
        $media = FFMpeg::fromDisk('public')->open($sound->file);
        $rawDuration = $media->getDurationInSeconds();

        // Create the clip filter
        $start = TimeCode::fromSeconds($offset);
        $end = TimeCode::fromSeconds($duration);
        $clipFilter = new AudioClipFilter($start, $end);

        // Create the silence filter
        $silenceFilter = new CustomFilter('adelay=delays=' . $silenceDuration . 's:all=1');

        // Create the volume filter (+50% by default)
        $volumeFilter = new CustomFilter('volume=1.5');

        // Create a sound file of the right duration, by clipping or concatenating
        // Fading filter is directly applied to the sound
        if ($rawDuration > $duration) {
            $media->addFilter($clipFilter)
                ->addFilter($volumeFilter)
                ->addFilter($silenceFilter)
                ->export()
                ->toDisk('public')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save($newSampleName);
        } elseif ($rawDuration < $duration) {
            $concatCount = ceil($duration / $rawDuration);
            $tempName = 'samples/' . Str::random(10) . '-temp.mp3';
            $concatSounds = [];
            for ($i = 0; $i < $concatCount ; $i++) {
                array_push($concatSounds, $sound->file);
            }
            FFMpeg::fromDisk('public')
                            ->open($concatSounds)
                            ->export()
                            ->concatWithoutTranscoding()
                            ->save($tempName);
            $rawDuration = FFMpeg::fromDisk('public')->open($tempName)->getDurationInSeconds();

            FFMpeg::fromDisk('public')
                        ->open($tempName)
                        ->addFilter($clipFilter)
                        ->addFilter($silenceFilter)
                        ->export()
                        ->toDisk('public')
                        ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                        ->save($newSampleName);
            Storage::delete('public/' . $tempName);
        }

        $sound->faded_track = $newSampleName;
        $sound->save();
    }

    public static function generateRing(array $sounds, ?int $extraWakeUp = null, string $name = ''): string
    {
        // Working temp directory
        $tempFolder = Str::random(10);

        // Temp files list
        $tempFiles = [];

        // Create temp sound files with requested volumes
        $soundsWithAdaptedVolumes = [];
        foreach ($sounds as $sound) {
            $tempFileName = $tempFolder . '/' . Str::slug($sound[0]) . '.mp3';
            $volumeFilter = new CustomFilter('volume=' . $sound[1] / 10);
            $adaptedSound = FFMpeg::fromDisk('public')
                ->open($sound[0])
                ->addFilter($volumeFilter)
                ->export()
                ->toDisk('public')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save($tempFileName);
            array_push($soundsWithAdaptedVolumes, $tempFileName);
            array_push($tempFiles, $tempFileName);
        }

        // Merge files with adapted volumes
        $tempMergeWithoutWakeUpName = $tempFolder . '/temp-merge-without-wakeup.mp3';
        if (count($soundsWithAdaptedVolumes) > 1) {
            $inputString = '';
            for ($i = 0; $i < count($soundsWithAdaptedVolumes); $i++) {
                $inputString .= '[' . $i . ':0]';
            }
            FFMpeg::fromDisk('public')
                    ->open($soundsWithAdaptedVolumes)
                    ->export()
                    ->addFilter($inputString, 'amerge=inputs=' . count($soundsWithAdaptedVolumes), '')
                    ->addFormatOutputMapping(new \FFMpeg\Format\Audio\Mp3, Media::make('public', $tempMergeWithoutWakeUpName), [''])
                    ->save();
        } else {
            FFMpeg::fromDisk('public')
                    ->open($soundsWithAdaptedVolumes[0])
                    ->export()
                    ->save($tempMergeWithoutWakeUpName);
        }
        array_push($tempFiles, $tempMergeWithoutWakeUpName);

        // Create final file with additional wakeup ring
        $ringName = 'ring';
        if ($name !== '') {
            $ringName = Str::slug($name);
        }
        // $tempWakeUpFullTime = $tempFolder . '/temp-wakeup-with-silence.mp3';
        // $tempMergeWithWakeUpName = $tempFolder . '/temp-merge-with-wakeup.mp3';
        if ($extraWakeUp) {
            $wakeupSound = Sound::find($extraWakeUp);
            FFMpeg::fromDisk('public')
                    ->open([$tempMergeWithoutWakeUpName, $wakeupSound->faded_track])
                    ->export()
                    ->addFilter('[0:0][1:0]', 'amerge=inputs=2', '')
                    ->addFormatOutputMapping(new \FFMpeg\Format\Audio\Mp3, Media::make('public', $tempFolder . '/' . $ringName . '.mp3'), [''])
                    ->save();
        } else {
            FFMpeg::fromDisk('public')
                    ->open($tempMergeWithoutWakeUpName)
                    ->export()
                    ->save($tempFolder . '/' . $ringName . '.mp3');
        }

        foreach ($tempFiles as $tempFile) {
            Storage::delete('public/' . $tempFile);
        }

        $ring = new Ring;
        $ring->file = $tempFolder . '/' . $ringName . '.mp3';
        if ($name === '') {
            $ring->name = 'ring-' . sprintf("%03d", auth()->user()->rings()->count() + 1);
        } else {
            $ring->name = strtolower($name);
        }
        if (auth()->check()) {
            $ring->user_id = auth()->id();
        }
        $ring->save();

        return $tempFolder . '/' . $ringName . '.mp3';
    }

    // Old ring generation code
    // array_push($tempFiles, $tempMergeWithWakeUpName);

    // Duplicate file without wakeup with progressive volume increase
    // $volumeFilter = new CustomFilter('volume=0.08');
    // FFMpeg::fromDisk('public')
    //     ->open($tempMergeWithoutWakeUpName)
    //     ->addFilter($volumeFilter)
    //     ->export()
    //     ->toDisk('public')
    //     ->inFormat(new \FFMpeg\Format\Audio\Mp3)
    //     ->save($tempFolder . '/lowest.mp3');
    // array_push($tempFiles, $tempFolder . '/lowest.mp3');

    // $volumeFilter = new CustomFilter('volume=0.4');
    // FFMpeg::fromDisk('public')
    //     ->open($tempMergeWithoutWakeUpName)
    //     ->addFilter($volumeFilter)
    //     ->export()
    //     ->toDisk('public')
    //     ->inFormat(new \FFMpeg\Format\Audio\Mp3)
    //     ->save($tempFolder . '/lower.mp3');
    // array_push($tempFiles, $tempFolder . '/lower.mp3');

    // Final file creation
    // $ringName = 'ring';
    // if ($name !== '') {
    //     $ringName = Str::slug($name);
    // }
    // FFMpeg::fromDisk('public')
    //     ->open([$tempFolder . '/lowest.mp3', $tempFolder . '/lower.mp3', $tempMergeWithWakeUpName])
    //     ->export()
    //     ->concatWithoutTranscoding()
    //     ->save($tempFolder . '/' . $ringName . '.mp3');


    // To try for relative volumes adjustments
    // To Try
    // $conversion = FFMpeg::fromDisk('local')
    //     ->open(['sounds'])
    //     ->export()
    //     // For loop here
    //     // Push outputs to array at each iteration
    //     ->addFilter('[0:a]', 'volume=xx', '[a0]') // One filter for each sound track
    //     ->addFilter('[1:a]', 'volume=xx', '[a1]')  // $in, $parameters, $out
    //     // End for
    //     ->addFormatOutputMapping(new X264, Media::make('local', 'mergedwithvolume.mp3'), ['[a0]', '[a1]']) // recombine all outputs
    //     ->save();
}
