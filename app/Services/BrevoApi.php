<?php

namespace App\Services;

use Brevo\Client\Api\ContactsApi;
use Brevo\Client\Model\CreateUpdateContactModel;
use Brevo\Client\Model\GetLists;
use GuzzleHttp;
use Hofmannsven\Brevo\Facades\Brevo;
use Illuminate\Support\Facades\Http;

class BrevoApi
{
    public static function getLists(): GetLists
    {
        $apiInstance = self::createContactsApiInstance();
        $lists = $apiInstance->getLists();

        return $lists;
    }

    public static function createContact(string $email, string $firstName, string $lastName, $dateOfBirth): CreateUpdateContactModel|null
    {
        $list_id = (int) config('brevo.contact_list_id');
        $apiInstance = self::createContactsApiInstance();
        $contact = new \Brevo\Client\Model\CreateContact([
            'email' => $email,
            'updateEnabled' => true,
            'attributes' => (object) [ 'PRENOM' => $firstName, 'NOM' => $lastName, 'DATE_NAISSANCE' => $dateOfBirth ],
            'listIds' => [$list_id],
       ]);

        $createdContact = $apiInstance->createContact($contact);

        return $createdContact;
    }

    public static function deleteContact(string $email): void
    {
        $apiInstance = self::createContactsApiInstance();

        $createdContact = $apiInstance->deleteContact($email);
    }

    public static function createTrackEvent(string $email, string $first_name, string $last_name): void
    {
        $response = Http::withHeaders([
            'accept' => 'application/json',
            'content-type' => 'application/json',
            'ma-key' => config('brevo.api_key'),
        ])->post('https://in-automate.brevo.com/api/v2/trackEvent', [
            'email' => $email,
            'event' => 'new_user_registered',
            'eventdata' => [
                "data" => [
                    "username" => ucfirst($first_name) . ' ' . ucfirst($last_name),
                    "email" => $email,
                ]
            ]
        ]);
    }

    public static function sendRegistrationEmail(string $email, string $first_name, string $last_name): void
    {
        $apiInstance = self::createEmailsApiInstance();

        $emailContent = new \Brevo\Client\Model\SendSmtpEmail;
        $emailContent['to'] = [['email' => $email, 'name' => $first_name . ' ' . $last_name]];
        $emailContent['templateId'] = (int) config('brevo.registration_template_id');
        $emailContent['params'] = ['PRENOM' => $first_name, 'NOM' => $last_name];

        $apiInstance->sendTransacEmail($emailContent);
    }

    protected static function createContactsApiInstance(): ContactsApi
    {
        $config = Brevo::getConfiguration();
        $apiInstance = new \Brevo\Client\Api\ContactsApi(
            new GuzzleHttp\Client,
            $config
        );
        return $apiInstance;
    }

    protected static function createEmailsApiInstance()
    {
        $config = Brevo::getConfiguration();
        $apiInstance = new \Brevo\Client\Api\TransactionalEmailsApi(
            new GuzzleHttp\Client,
            $config
        );
        return $apiInstance;
    }
}
