<?php

namespace App\Filament\Resources\QuestionResource\Pages;

use App\Enums\QuestionTypes;
use App\Filament\Resources\QuestionResource;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Support\Str;

class CreateQuestion extends CreateRecord
{
    protected static string $resource = QuestionResource::class;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        if (in_array($data['type'], [QuestionTypes::MultipleChoice, QuestionTypes::Radio])) {
            $index = 0;
            $newOptions = [];
            foreach ($data['options'] as $option) {
                $newOption = [];
                $newOption['key'] = Str::random(5) . '-' . $index;
                $newOption['answer'] = $option['answer'];
                array_push($newOptions, $newOption);
                $index++;
            }
            $data['options'] = $newOptions;
        }

        return $data;
    }
}
