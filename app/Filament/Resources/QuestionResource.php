<?php

namespace App\Filament\Resources;

use App\Enums\QuestionTypes;
use App\Filament\Resources\QuestionResource\Pages;
use App\Models\Question;
use Filament\Forms;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class QuestionResource extends Resource
{
    protected static ?string $model = Question::class;

    protected static ?string $navigationIcon = 'heroicon-o-question-mark-circle';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Textarea::make('question')
                    ->required()
                    ->maxLength(2000)
                    ->columnSpanFull(),
                Forms\Components\Select::make('type')
                    ->required()
                    ->options(QuestionTypes::asSelectArray())
                    ->default('multiple')
                    ->columnSpan('full')
                    ->live(),
                Repeater::make('options')
                    ->schema([
                        TextInput::make('answer')->required(),
                    ])
                    ->hidden(fn (Get $get): bool => $get('type') == QuestionTypes::Range)
                    ->minItems(2)
                    ->maxItems(10)
                    ->defaultItems(2)
                    ->columnSpan('full')
                    ->disabledOn('edit'),
                Forms\Components\Toggle::make('visible')
                    ->label('Visibility on the website')
                    ->required(),
                TextInput::make('position')
                    ->numeric()
                    ->default(Question::count() + 1)
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\IconColumn::make('visible')
                    ->boolean()
                    ->sortable(),
                Tables\Columns\TextColumn::make('question')
                    ->limit(75)
                    ->searchable(),
                Tables\Columns\TextColumn::make('type')
                    ->sortable(),
                Tables\Columns\TextColumn::make('position')
                    ->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListQuestions::route('/'),
            'create' => Pages\CreateQuestion::route('/create'),
            'edit' => Pages\EditQuestion::route('/{record}/edit'),
        ];
    }
}
