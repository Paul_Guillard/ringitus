<?php

namespace App\Filament\Resources;

use App\Enums\SoundCategories;
use App\Filament\Resources\SoundResource\Pages;
use App\Models\Sound;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\ViewField;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ViewColumn;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Table;

class SoundResource extends Resource
{
    protected static ?string $model = Sound::class;

    protected static ?string $navigationIcon = 'heroicon-s-musical-note';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                ViewField::make('file')
                            ->hiddenOn('create')
                            ->columnSpan('full')
                            ->view('filament.components.audio-player'),
                FileUpload::make('raw_file')
                            ->label('Sound file - Accepted file types: mp3, wav, webm, aac, ogg - Max Size: 40Mb')
                            ->columnSpan('full')
                            ->directory('raw_sounds')
                            ->required()
                            ->hiddenOn('edit')
                            ->acceptedFileTypes(['application/octet-stream', 'audio/mpeg', 'audio/wav', 'audio/webm', 'audio/aac', 'audio/ogg', 'audio/mp4', 'audio/x-m4a']),
                TextInput::make('name')->label('Track Name')->columnSpan('full'),
                Select::make('category')
                        ->options(SoundCategories::asSelectArray())
                        ->default(SoundCategories::Tinnitus)
                        ->columnSpan('full')
                        ->required(),
                Toggle::make('is_active')->label('Display sound on the website'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')->label('Track Name')->searchable()->sortable(),
                IconColumn::make('is_active')
                                ->sortable()
                                ->icon(fn (string $state): string => match ($state) {
                                    '0' => 'heroicon-s-no-symbol',
                                    '1' => 'heroicon-s-check-circle',
                                })
                                ->color(fn (string $state): string => match ($state) {
                                    '0' => 'danger',
                                    '1' => 'success',
                                }),
                TextColumn::make('category')->label('Track Category')->sortable(),
                ViewColumn::make('file')->label('Play sound')->view('filament.components.audio-player')
            ])
            ->filters([
                Filter::make('name'),
                Filter::make('category'),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSounds::route('/'),
            'create' => Pages\CreateSound::route('/create'),
            'edit' => Pages\EditSound::route('/{record}/edit'),
        ];
    }
}
