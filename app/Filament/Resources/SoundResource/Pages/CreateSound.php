<?php

namespace App\Filament\Resources\SoundResource\Pages;

use App\Enums\SoundCategories;
use App\Filament\Resources\SoundResource;
use App\Services\SoundManager;
use Filament\Resources\Pages\CreateRecord;

class CreateSound extends CreateRecord
{
    protected static string $resource = SoundResource::class;

    protected function afterCreate(): void
    {
        // Convert to mp3
        SoundManager::convertToMp3($this->record);

        // Create 1min sample
        SoundManager::createSample($this->record, 60, 0);

        // Create 3min faded sample
        if ($this->record->category === SoundCategories::Wakeup) {
            SoundManager::createFullWakeUp($this->record);
        } else {
            SoundManager::createFadedTrack($this->record, 180);
        }
    }
}
