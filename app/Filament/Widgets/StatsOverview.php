<?php

namespace App\Filament\Widgets;

use App\Enums\UserRoles;
use App\Models\Message;
use App\Models\Ring;
use App\Models\User;
use Carbon\Carbon;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
// use Filament\Widgets\StatsOverviewWidget\Card;
use Filament\Widgets\StatsOverviewWidget\Stat;

class StatsOverview extends BaseWidget
{
    protected function getStats(): array
    {
        return [
            Stat::make('Users', User::where('role', UserRoles::User)->count())
                ->chart([
                    User::whereDate('created_at', Carbon::today()->subDays(6))->count(),
                    User::whereDate('created_at', Carbon::today()->subDays(5))->count(),
                    User::whereDate('created_at', Carbon::today()->subDays(4))->count(),
                    User::whereDate('created_at', Carbon::today()->subDays(3))->count(),
                    User::whereDate('created_at', Carbon::today()->subDays(2))->count(),
                    User::whereDate('created_at', Carbon::today()->subDays(1))->count(),
                    User::whereDate('created_at', Carbon::today())->count(),
                ])
                ->color(User::whereDate('created_at', '>=', Carbon::today()->subDays(6))->count() > 0 ? 'success' : 'danger'),
            Stat::make('Generated Rings', Ring::count())
                ->chart([
                    Ring::whereDate('created_at', Carbon::today()->subDays(6))->count(),
                    Ring::whereDate('created_at', Carbon::today()->subDays(5))->count(),
                    Ring::whereDate('created_at', Carbon::today()->subDays(4))->count(),
                    Ring::whereDate('created_at', Carbon::today()->subDays(3))->count(),
                    Ring::whereDate('created_at', Carbon::today()->subDays(2))->count(),
                    Ring::whereDate('created_at', Carbon::today()->subDays(1))->count(),
                    Ring::whereDate('created_at', Carbon::today())->count(),
                ])
                ->color(Ring::whereDate('created_at', '>=', Carbon::today()->subDays(6))->count() > 0 ? 'success' : 'danger'),
            Stat::make('New Messages', Message::where('is_read', false)->count())
                ->chart([
                    Message::whereDate('created_at', Carbon::today()->subDays(6))->count(),
                    Message::whereDate('created_at', Carbon::today()->subDays(5))->count(),
                    Message::whereDate('created_at', Carbon::today()->subDays(4))->count(),
                    Message::whereDate('created_at', Carbon::today()->subDays(3))->count(),
                    Message::whereDate('created_at', Carbon::today()->subDays(2))->count(),
                    Message::whereDate('created_at', Carbon::today()->subDays(1))->count(),
                    Message::whereDate('created_at', Carbon::today())->count(),
                ])
                ->color(Message::whereDate('created_at', '>=', Carbon::today()->subDays(6))->count() > 0 ? 'success' : 'danger'),
        ];
    }
}
