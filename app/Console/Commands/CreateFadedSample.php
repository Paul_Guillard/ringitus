<?php

namespace App\Console\Commands;

use App\Enums\SoundCategories;
use App\Models\Sound;
use App\Services\SoundManager;
use Illuminate\Console\Command;

class CreateFadedSample extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-sounds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates faded sound for sounds present in the database before the sound generation update';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        echo "Converting sounds with no faded file...\n";
        foreach (Sound::whereNull('faded_track')->get() as $sound) {
            if ($sound->category === SoundCategories::Wakeup) {
                SoundManager::createFullWakeUp($sound);
            } else {
                SoundManager::createFadedTrack($sound, 180);
            }
            echo "Sound " . $sound->name . " updated!\n";
        }
        echo "All sounds updated!\n";
        echo "Cleaning up non compliant files...\n";
        foreach (Sound::whereNull('sample')->get() as $sound) {
            $sound->delete();
            echo "Sound " . $sound->name . " deleted!\n";
        }
        echo "Clean-up finished!\n";
    }
}
