<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LintCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lint {--fix}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activates Laravel Duster to check the code. Use the --fix option to automatically correct the detected issues.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        echo "Laravel Duster launched...\n";
        if ($this->option('fix')) {
            $result = shell_exec('./vendor/bin/duster fix');
        } else {
            $result = shell_exec('./vendor/bin/duster lint');
        }
        echo $result . "\n";
        echo "Laravel Duster executed!\n";
    }
}
