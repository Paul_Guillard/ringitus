<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

final class SoundCategories extends Enum
{
    public const Nature = 'nature';
    public const Tinnitus = 'tinnitus';
    public const Wakeup = 'wakeup';
    public const Suggestion = 'suggestion';
}
