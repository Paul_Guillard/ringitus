<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

final class QuestionTypes extends Enum
{
    public const MultipleChoice = 'multiple';
    public const Radio = 'radio';
    public const Range = 'range';
}
