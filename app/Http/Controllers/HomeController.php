<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class HomeController extends Controller
{
    public function show(): View
    {
        return view('welcome');
    }

    public function showRingitus(): View
    {
        return view('ringitus');
    }
}
