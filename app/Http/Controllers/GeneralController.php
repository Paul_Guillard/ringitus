<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class GeneralController extends Controller
{
    public function showConditions(): View
    {
        return view('footer.general-conditions');
    }

    public function showLegal(): View
    {
        return view('footer.legal-mentions');
    }

    public function showDataPolicy(): View
    {
        return view('footer.data-policy');
    }

    public function showContact(): View
    {
        return view('footer.contact');
    }
}
