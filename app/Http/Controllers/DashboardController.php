<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class DashboardController extends Controller
{
    public function show(): View
    {
        $questions = auth()->user()->questions;

        return view('dashboard', [
            'questions' => $questions,
        ]);
    }
}
