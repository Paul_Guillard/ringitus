<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class SoundsController extends Controller
{
    public function index(): View
    {
        return view('samples');
    }
}
